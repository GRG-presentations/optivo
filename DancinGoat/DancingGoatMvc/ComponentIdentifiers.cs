﻿namespace DancingGoat
{
    /// <summary>
    /// Encapsulated identifiers of components without a dedicated controller.
    /// </summary>
    public static class ComponentIdentifiers
    {
        // Widgets
        public const string CTA_BUTTON_WIDGET = "DancingGoat.General.CTAButtonWidget";
        public const string TESTIMONIAL_WIDGET = "DancingGoat.LandingPage.TestimonialWidget";
        public const string HOME_MAP_WIDGET = "DancingGoat.General.HomeMapWidget";
        public const string HOME_DETAILS_WIDGET = "DancingGoat.General.HomeDetailsWidget";
        public const string HOME_BRAND_WIDGET = "DancingGoat.General.HomeBrandWidget";
        public const string HOME_GALLERY_WIDGET = "DancingGoat.General.HomeGalleryWidget";
        public const string HOME_REGISTER_WIDGET = "DancingGoat.General.HomeRegisterWidget";
        public const string HOME_BROCHURE_WIDGET = "DancingGoat.General.HomeBrochureWidget";
        public const string HOME_SPECIFICATION_WIDGET = "DancingGoat.General.HomeSpecificationWidget";
        public const string VIDEO_IMAGE_BOX_WIDGET = "DancingGoat.General.VideoImageBoxWidget";
        public const string TESTIMONIALS_FEATURE_WIDGET = "DancingGoat.General.TestimonialsFeatureWidget";
        public const string ACCORDION_CONTAINER_WIDGET = "DancingGoat.General.AccordionContainerWidget";
        public const string ACCORDION_ITEM_WIDGET = "DancingGoat.General.AccordionItemWidget";
        public const string MORE_FROM_OPTIVO_WIDGET = "DancingGoat.General.MoreFromOptivoWidget";
        public const string CONTACT_CONTAINER_WIDGET = "DancingGoat.General.ContactContainerWidget";
        public const string CONTACT_CARD_WIDGET = "DancingGoat.General.ContactCardWidget";
        public const string CONTACT_TIME_WIDGET = "DancingGoat.General.ContactTimeWidget";
        public const string TWO_IMAGE_WIDGET = "DancingGoat.General.TwoImageWidget";
        public const string MEET_THE_TEAM_WIDGET = "DancingGoat.General.MeetTheTeamWidget";
        public const string TESTIMONIALS_LIST_WIDGET = "DancingGoat.General.TestimonialsListWidget";
        public const string FEATURED_WIDGET = "DancingGoat.General.FeaturedWidget";

        // Sections
        public const string SINGLE_COLUMN_SECTION = "DancingGoat.SingleColumnSection";
        public const string TWO_COLUMN_SECTION = "DancingGoat.TwoColumnSection";
        public const string THREE_COLUMN_SECTION = "DancingGoat.ThreeColumnSection";

        // Page templates
        public const string LANDING_PAGE_SINGLE_COLUMN_TEMPLATE = "DancingGoat.LandingPageSingleColumn";
        public const string EDUCATIONAL_PAGE_TEMPLATE = "DancingGoat.EducationalPage";
        public const string MAIN_PAGE_TEMPLATE = "DancingGoat.MainPage";
        public const string BLOG_TEMPLATE = "DancingGoat.Blog";
        public const string TESTIMONIAL_TEMPLATE = "DancingGoat.Testimonial";
        public const string LISTING_TEMPLATE = "DancingGoat.Listing";
        public const string FAQ_TEMPLATE = "DancingGoat.Faq";
        public const string TESTIMONIALS_TEMPLATE = "DancingGoat.Testimonials";
        public const string SEARCH_TEMPLATE = "DancingGoat.Search";
        public const string CONTACT_PAGE_TEMPLATE = "DancingGoat.ContactPage";
    }
}