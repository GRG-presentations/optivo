﻿using DancingGoat.Models.EducationalPage;
using DancingGoat.Models.FaqPage;
using DancingGoat.Models.References;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DancingGoat.Models.BuyingGuides
{
    public class BuyingGuidesViewModel
    {
        public IEnumerable<BuyingGuidesSectionViewModel> Sections { get; set; }
        public IEnumerable<EducationalPageSectionViewModel> EducationalsSections { get; set; }
        public IEnumerable<FaqSectionViewModel> FaqSections { get; set; }

        public ReferenceViewModel Reference { get; set; }
    }
}