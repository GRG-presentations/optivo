﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.EducationalPage;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.BuyingGuides
{
    public class BuyingGuidesSectionViewModel
    {
        public string Heading { get; set; }


        public string Text { get; set; }


        public string ImagePath { get; set; }
        public IEnumerable<EducationalPageSectionViewModel> EducationalPages { get; set; }


        public static BuyingGuidesSectionViewModel GetViewModel(BuyingGuidesSection buyingGuidesSection, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            /*IEnumerable<EducationalPageSectionViewModel>  EducationalPagess = buyingGuidesSection.Fields.EducationalPages.OfType<EducationalPageSectionViewModel>().Select(section => EducationalPageSectionViewModel.GetViewModel(section, pageUrlRetriever, attachmentUrlRetriever));*/
            
            return new BuyingGuidesSectionViewModel
            {
                Heading = buyingGuidesSection.Fields.Heading,
                Text = buyingGuidesSection.Fields.Text,
                ImagePath = buyingGuidesSection.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(buyingGuidesSection.Fields.Image).RelativePath,
                EducationalPages = buyingGuidesSection.Fields.EducationalPages.OfType<EducationalPageSection>().Select(section => EducationalPageSectionViewModel.GetViewModel(section, pageUrlRetriever, attachmentUrlRetriever)),
            };
        }
    }
}