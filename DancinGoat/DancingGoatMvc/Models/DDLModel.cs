﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCalling.Models
{
    public class DDLModel
    {
        public int? id { get; set; }
        public bool? active { get; set; }
        public string name { get; set; }
    }
}
