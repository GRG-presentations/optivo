﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCalling.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 

    public class finalResponse
    {
        public List<Response> Responses { get; set; }
    }
    public class DevelopmentPreference
    {
        public int? leadDevelopmentId { get; set; }
        public bool? marketingApproval { get; set; } 
        public string marketingName { get; set; }
    }

    public class PreviousAddress
    {
        public int? previousAddressId { get; set; }
        public DateTime? dateMovedIn { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string addressLine4 { get; set; }
        public string postcode { get; set; }
    }

    public class Response
    {
        public bool? canEmail { get; set; } 
        public bool? canPhone { get; set; } 
        public bool? canText { get; set; } 
        public bool? marketingMaterial { get; set; } 
        public bool? estateAgentApproval { get; set; } 
        public bool? newsletter { get; set; } 
        public bool? privacyPolicy { get; set; } 
        public List<DevelopmentPreference> developmentPreferences { get; set; }
        public int? id { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public string emailAddress { get; set; }
        public string middleName { get; set; }
        public string previousSurname { get; set; }
        public int? genderTypeId { get; set; }
        public int? maritalStatusId { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public DateTime? dateMovedIn { get; set; }
        public string mobileTel { get; set; }
        public string workTel { get; set; }
        public string homeTel { get; set; }
        public string nationalInsurance { get; set; }
        public string currentAddressLine1 { get; set; }
        public string currentAddressLine2 { get; set; }
        public string currentAddressLine3 { get; set; }
        public string currentAddressLine4 { get; set; }
        public string currentAddressPostcode { get; set; }
        public List<PreviousAddress> previousAddresses { get; set; }
        public string correspondenceAddressLine1 { get; set; }
        public string correspondenceAddressLine2 { get; set; }
        public string correspondenceAddressLine3 { get; set; }
        public string correspondenceAddressLine4 { get; set; }
        public string correspondenceAddressPostcode { get; set; }
        public bool? modWorker { get; set; } 
        public bool? haWorker { get; set; } 
        public int? jobStatusId { get; set; }
        public string jobTitle { get; set; }
        public string employerName { get; set; }
        public string workAddressLine1 { get; set; }
        public string workAddressLine2 { get; set; }
        public string workAddressLine3 { get; set; }
        public string workAddressLine4 { get; set; }
        public string workAddressPostcode { get; set; }
        public DateTime? employerStartDate { get; set; }
        public int? annualSalary { get; set; }
        public int? monthlyOtherIncome { get; set; }
        public int? monthlyDisabilityAllowance { get; set; }
        public int? monthlyMaintenance { get; set; }
        public int? monthlyTaxCredits { get; set; }
        public int? monthlyRent { get; set; }
        public int? creditCardBalance { get; set; }
        public int? outstandingLoanAmount { get; set; }
        public string loanPurpose { get; set; }
        public DateTime? loanEndDate { get; set; }
        public int? monetaryGift { get; set; }
        public int? annualBonus { get; set; }
        public int? savings { get; set; }
        public int? deposit { get; set; }
        public bool? canRaiseDeposit { get; set; } 
        public bool? hasPaymentsKeptUp { get; set; } 
        public bool? hasRentKeptUp { get; set; } 
        public bool? hasBeenBankruptOrCCJ { get; set; } 
        public bool? hasEnteredintoIVA { get; set; } 
        public bool? hasIVABeenAccepted { get; set; } 
        public bool? ownsProperty { get; set; } 
        public int? homeBoroughId { get; set; }
        public int? workBoroughId { get; set; }
        public int? currentAccommodationTypeId { get; set; }
        public string councilWaitingListRef { get; set; }
        public int? currentBeds { get; set; }
        public bool? laRegistered { get; set; } 
        public bool? homeBuyRegistered { get; set; } 
        public bool? helpToBuyRegistered { get; set; } 
        public string helpToBuyReference { get; set; }  
        public bool? consideredDisabled { get; set; } 
        public bool? registeredDisabled { get; set; } 
        public bool? wheelchairUser { get; set; } 
        public bool? havePets { get; set; } 
        public string accessibilityNeeds { get; set; }
        public int? religionTypeId { get; set; }
        public int? sexualityTypeId { get; set; }
        public int? nationalityTypeId { get; set; }
        public int? ethnicityTypeId { get; set; }
    }



}
