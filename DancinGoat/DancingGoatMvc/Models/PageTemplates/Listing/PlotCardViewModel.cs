﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class PlotCardViewModel
    {
        public int? minBedrooms { get; set; }
        public double? pricesFrom { get; set; }
        public int? shareFrom30 { get; set; }
        public string? minDespositFrom { get; set; }
        public string? totalMonthlyCostFrom { get; set; }
        public string? monthlyRentFrom { get; set; }
        public int? tenureTypeId { get; set; }
        public int? propertyId { get; set; }
        public bool FeaturedProperty { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public string Image { get; set; }

        public static PlotCardViewModel GetViewModel(Plots plot, IEnumerable<PlotsViewModel> PlotData)
        {
            bool fp = false;
            string tag1 = null;
            string tag2 = null;
            string img = null;
            foreach (var item in PlotData) { 
                if(item.PlotTypeId == plot.plotNumber.ToString())
                {
                    fp = item.FeaturedProperty;
                    tag1 = item.Tag1;
                    tag2 = item.Tag2;
                    img = item.Image;
                }
            }
            return new PlotCardViewModel
            {
                minBedrooms = plot.beds,
                pricesFrom = plot.latestValuation,
                shareFrom30 = plot.minShare,
                tenureTypeId = plot.tenureTypeId,
                propertyId = plot.propertyId,
                FeaturedProperty = fp,
                Tag1 = tag1,
                Tag2 = tag2,
                Image = img
            };
        }
    }
}