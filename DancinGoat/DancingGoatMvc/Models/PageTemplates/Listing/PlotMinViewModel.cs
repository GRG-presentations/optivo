﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class PlotMinViewModel
    {
        public int? minBedrooms { get; set; }
        public double? pricesFrom { get; set; }
        public int? shareFrom30 { get; set; }
        public string? minDespositFrom { get; set; }
        public string? totalMonthlyCostFrom { get; set; }
        public string? monthlyRentFrom { get; set; }

        public static PlotMinViewModel GetViewModel(List<Plots> plot)
        {
            int? minBeds = plot.Count() > 0 ? plot[0].beds : 0;
            int? minShare = plot.Count() > 0 ? plot[0].minShare : 0;
            object? minCmsMonthlyRent = plot.Count() > 0 ? plot[0].cmsMonthlyRent : 0;
            double? minPrices = plot.Count() > 0 ? plot[0].latestValuation : 0;
            if(plot.Count() > 0)
            {
                plot.ForEach(item =>
                {
                    if (item.beds < minBeds)
                    {
                        minBeds = item.beds;
                    }
                    if (item.minShare < minShare)
                    {
                        minShare = item.minShare;
                    }
                    /*if (item.cmsMonthlyRent < minCmsMonthlyRent)
                    {
                        minCmsMonthlyRent = item.cmsMonthlyRent;
                    }*/
                    if (item.latestValuation < minPrices)
                    {
                        minPrices = item.latestValuation;
                    }
                });
            }
            return new PlotMinViewModel
            {
                minBedrooms = minBeds,
                pricesFrom = minPrices,
                shareFrom30 = minShare
            };
        }
    }
}