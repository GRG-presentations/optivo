﻿
using System.Collections.Generic;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Linq;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class InfoViewModel
    {
        public string PricesFrom { get; set; }
        public string DevelopmentId { get; set; }


        public string Title { get; set; }


        public string Location { get; set; }


        public string Bedrooms { get; set; }


        public string Text { get; set; }
    }
    public class ListViewModel
    {
        public string PricesFrom { get; set; }
        public string DevelopmentId { get; set; }


        public string Title { get; set; }


        public string Location { get; set; }


        public string Bedrooms { get; set; }


        public string Text { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public bool FeaturedProperty { get; set; }

        public IEnumerable<PlotCardViewModel> CardData { get; set; }


        public static ListViewModel GetViewModel(Listing listing, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            // Applies the width specified through the parameter of the form control if it is valid
            const string url = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/get-properties-for-development/";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + listing.Fields.SelectDevelopment.ToString());
            WebHeaderCollection myWebHeaderCollection = request.Headers;
            myWebHeaderCollection.Add("authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.Method = "GET";

            var webResponse = request.GetResponse();
            var webStream = webResponse.GetResponseStream();
            var responseReader = new StreamReader(webStream);
            var response = responseReader.ReadToEnd();
            IEnumerable<Plot> plots = PlotProvider.GetPlots()
                        .WhereStartsWith("NodeAliasPath", "/Listings/" + listing.Fields.SelectDevelopment.ToString());
            IEnumerable<PlotsViewModel> PlotData = plots.Select(plot => PlotsViewModel.GetViewModel((Plot)plot, pageUrlRetriever, attachmentUrlRetriever));
            IEnumerable<Plots> model1 = JsonConvert.DeserializeObject<List<Plots>>(response);

            IEnumerable<PlotCardViewModel> cardData = model1.Select(item => PlotCardViewModel.GetViewModel(item, PlotData));

            return new ListViewModel
            {
                Text = listing.Fields.Text,
                Title = listing.Fields.Title,
                DevelopmentId = listing.Fields.SelectDevelopment,
                Location = listing.Fields.Location,
                CardData = cardData
            };
        }
    }
}