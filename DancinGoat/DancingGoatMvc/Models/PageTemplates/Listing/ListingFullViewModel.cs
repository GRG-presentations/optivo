﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class Plots
    {
        public int? propertyId { get; set; }
        public int? developmentId { get; set; }
        public int? plotStatusId { get; set; }
        public int? plotTypeId { get; set; }
        public int? tenureTypeId { get; set; }
        public int? beds { get; set; }
        public string? floor { get; set; }
        public string? plotNumber { get; set; }
        public double? area { get; set; }
        public double? rentPercentage { get; set; }
        public double? serviceCharge { get; set; }
        public bool? parking { get; set; }
        public string? parkingInfo { get; set; }
        public bool? cmsIsLive { get; set; }
        public bool? cmsPricesLive { get; set; }
        public double? latestValuation { get; set; }
        public object? cmsSpend { get; set; }
        public object? cmsIncome { get; set; }
        public object? cmsMonthlyRent { get; set; }
        public object? cmsDeposit { get; set; }
        public object? specification { get; set; }
        public int? minShare { get; set; }
        public int? maxShare { get; set; }
        public string? buildingNumber { get; set; }
        public string? buildingName { get; set; }
        public string? streetNumber { get; set; }
        public string? streetName { get; set; }
        public string? town { get; set; }
        public string? county { get; set; }
        public string? postcode { get; set; }
    }
    public class ListingFullViewModel
    {
        public string PricesFrom { get; set; }
        public string DevelopmentId { get; set; }


        public string Title { get; set; }


        public string Location { get; set; }


        public string Bedrooms { get; set; }


        public string Text { get; set; }

        public IEnumerable<Plots> Plots { get; set; }
        public List<PlotsViewModel> PlotsData { get; set; }
        public PlotMinViewModel CardData { get; set; }


        public static ListingFullViewModel GetViewModel(Listing listing, IEnumerable<Plots> plot, IEnumerable<PlotsViewModel> plots, PlotMinViewModel card)
        {
            return new ListingFullViewModel
            {
                Text = listing.Fields.Text,
                Title = listing.Fields.Title,
                DevelopmentId = listing.Fields.SelectDevelopment,
                Location = listing.Fields.Location,
                Plots = plot,
                PlotsData = plots.ToList(),
                CardData = card
            };
        }
    }
}