﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class ListingViewModel
    {
        public string PricesFrom { get; set; }
        public string DevelopmentId { get; set; }


        public string Title { get; set; }


        public string Location { get; set; }


        public string Bedrooms { get; set; }


        public string Text { get; set; }


        public static ListingViewModel GetViewModel(Listing listing)
        {
            return new ListingViewModel
            {
                Text = listing.Fields.Text,
                Title = listing.Fields.Title,
                DevelopmentId = listing.Fields.SelectDevelopment,
                Location = listing.Fields.Location
            };
        }
    }
}