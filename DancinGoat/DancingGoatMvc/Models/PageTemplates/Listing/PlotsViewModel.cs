﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class PlotsViewModel
    {
        public string PlotTypeId { get; set; }
        public string PropertyId { get; set; }
        public string Floorplan { get; set; }
        public string EPC { get; set; }
        public string Image { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public bool FeaturedProperty { get; set; }

        public static PlotsViewModel GetViewModel(Plot plot, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new PlotsViewModel
            {
                Floorplan = plot.Fields.Floorplan == null ? null : attachmentUrlRetriever.Retrieve(plot.Fields.Floorplan).RelativePath,
                EPC = plot.Fields.Epc == null ? null : attachmentUrlRetriever.Retrieve(plot.Fields.Epc).RelativePath,
                Image = plot.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(plot.Fields.Image).RelativePath,
                Tag1 = plot.Fields.Tag1,
                Tag2 = plot.Fields.Tag2,
                FeaturedProperty = plot.Fields.FeaturedProperty,
                PlotTypeId = plot.Fields.TypeId,
                PropertyId = plot.DocumentName
            };
        }
    }
}