﻿using System;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class TeamMemberViewModel
    {
        public string Name { get; set; }

        public string CompanyPosition { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Image { get; set; }


        public static TeamMemberViewModel GetViewModel(TeamMember member, bool handleArticlePosition, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new TeamMemberViewModel
            {
                Name = member.Fields.Name,
                Image = member.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(member.Fields.Image).WithSizeConstraint(SizeConstraint.Height(301)).RelativePath,
                CompanyPosition = member.Fields.CompanyPosition,
                Phone = member.Fields.Phone,
                Email = member.Fields.Email
            };
        }
    }
}