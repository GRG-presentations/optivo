﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class TestimonialViewModel
    {
        public string TeaserPath { get; set; }


        public string Title { get; set; }


        public string Address { get; set; }


        public string Name { get; set; }


        public DateTime PublicationDate { get; set; }


        public string Text { get; set; }


        public IEnumerable<TeamMemberViewModel> Author { get; set; }


        public static TestimonialViewModel GetViewModel(Testimonial testimonial, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new TestimonialViewModel
            {
                PublicationDate = testimonial.PublicationDate,
                Author = testimonial.Fields.Author.OfType<TeamMember>().Select(author => TeamMemberViewModel.GetViewModel(author, true, pageUrlRetriever, attachmentUrlRetriever)),
                TeaserPath = testimonial.Fields.Teaser == null ? null : attachmentUrlRetriever.Retrieve(testimonial.Fields.Teaser).RelativePath,
                Text = testimonial.Fields.Text,
                Address = testimonial.Fields.Address,
                Name = testimonial.Fields.Name,
                Title = testimonial.Fields.Title
            };
        }
    }
}