﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.EducationalPage;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class SearchViewModel
    {
        public string BackgroundImagePath { get; set; }
        public string Heading { get; set; }
        public string Text { get; set; }
        public string MoreButtonText { get; set; }
        public string MoreButtonUrl { get; set; }

        public IEnumerable<ListViewModel> Listings { get; set; }


        public static SearchViewModel GetViewModel(SearchSection educationalPageSection, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever, IEnumerable<ListViewModel> Listing)
        {
            var link = educationalPageSection?.Fields.Link.FirstOrDefault();
            return educationalPageSection == null ? null : new SearchViewModel
            {
                BackgroundImagePath = attachmentUrlRetriever.Retrieve(educationalPageSection.Fields.Image).RelativePath,
                Heading = educationalPageSection.Fields.Heading,
                Text = educationalPageSection.Fields.Text,
                MoreButtonText = educationalPageSection.Fields.LinkText == null ? educationalPageSection.Fields.LinkText : "Learn More",
                MoreButtonUrl = pageUrlRetriever.Retrieve(educationalPageSection).RelativePath,
                Listings = Listing
            };
        }
    }
}