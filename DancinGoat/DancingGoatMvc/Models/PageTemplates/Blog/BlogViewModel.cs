﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using CMS.Taxonomy;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class BlogViewModel
    {
        public string TeaserPath { get; set; }


        public string Title { get; set; }


        public DateTime PublicationDate { get; set; }


        public string Text { get; set; }


        public IEnumerable<TeamMemberViewModel> Author { get; set; }
        public List<CategoryInfo> Categories { get; set; }


        public static BlogViewModel GetViewModel(Blog blog, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            List<CategoryInfo> CategoriesList = new List<CategoryInfo> { };
            if (blog.Categories.Count > 0)
            {
                foreach (var item in blog.Categories)
                {
                    CategoriesList.Add((CategoryInfo)item);
                }
            }
            return new BlogViewModel
            {
                PublicationDate = blog.PublicationDate,
                Author = blog.Fields.Author.OfType<TeamMember>().Select(author => TeamMemberViewModel.GetViewModel(author, true, pageUrlRetriever, attachmentUrlRetriever)),
                TeaserPath = blog.Fields.Teaser == null ? null : attachmentUrlRetriever.Retrieve(blog.Fields.Teaser).RelativePath,
                Text = blog.Fields.Text,
                Title = blog.Fields.Title,
                Categories = CategoriesList
            };
        }
    }
}