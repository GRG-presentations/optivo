﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.FaqPage;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.PageTemplates
{
    public class FaqViewModel
    {
        public string TeaserPath { get; set; }


        public string Title { get; set; }


        public DateTime PublicationDate { get; set; }


        public string Text { get; set; }


        public static FaqViewModel GetViewModel(FaqSection faq, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new FaqViewModel
            {
                TeaserPath = faq.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(faq.Fields.Image).RelativePath,
                Title = faq.Fields.Heading
            };
        }
    }
}