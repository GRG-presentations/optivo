﻿using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;
using System.Collections.Generic;

namespace DancingGoat.Models
{
    public class MediaLibraryViewModel
    {
        public string LibraryName { get; set; }

        public string LibrarySiteName { get; set; }

        public HashSet<string> AllowedImageExtensions { get; set; }
    }
}