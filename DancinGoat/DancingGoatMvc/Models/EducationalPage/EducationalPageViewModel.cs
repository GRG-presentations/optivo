﻿using System.Collections.Generic;
using DancingGoat.Models.References;

namespace DancingGoat.Models.EducationalPage
{
    public class EducationalPageViewModel
    {
        public IEnumerable<EducationalPageSectionViewModel> EducationalPageSections { get; set; }

        public ReferenceViewModel Reference { get; set; }
    }
}