﻿using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.EducationalPage
{
    public class EducationalPageSectionViewModel
    {
        public string BackgroundImagePath { get; set; }
        public string Heading { get; set; }
        public string Text { get; set; }
        public string MoreButtonText { get; set; }
        public string MoreButtonUrl { get; set; }

        public static EducationalPageSectionViewModel GetViewModel(EducationalPageSection educationalPageSection, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            var link = educationalPageSection?.Fields.Link.FirstOrDefault();
            return educationalPageSection == null ? null : new EducationalPageSectionViewModel
            {
                BackgroundImagePath = attachmentUrlRetriever.Retrieve(educationalPageSection.Fields.Image).RelativePath,
                Heading = educationalPageSection.Fields.Heading,
                Text = educationalPageSection.Fields.Text,
                MoreButtonText = educationalPageSection.Fields.LinkText == null ? educationalPageSection.Fields.LinkText : "Learn More",
                MoreButtonUrl = educationalPageSection.Parent.NodeAlias
            };
        }
    }
}