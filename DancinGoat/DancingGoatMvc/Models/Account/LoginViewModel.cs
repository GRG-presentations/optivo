﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DancingGoat.Models.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter your email address")]
        [DisplayName("Email Address")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string EmailAddress { get; set; }


        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter your password")]
        [DisplayName("Password")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string Password { get; set; }


        //[DisplayName("DancingGoatMvc.SignIn.StaySignedIn")]
        //public bool StaySignedIn { get; set; }
    }
}