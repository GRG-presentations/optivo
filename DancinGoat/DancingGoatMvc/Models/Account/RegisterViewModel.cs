﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using Kentico.Membership;

namespace DancingGoat.Models.Account
{
    public class RegisterViewModel
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Required")]
        [DisplayName("Email Address")]
        [EmailAddress(ErrorMessage = "DancingGoatMvc.General.InvalidEmail")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Required")]
        [DisplayName("Confirm Email Address")]
        [EmailAddress(ErrorMessage = "DancingGoatMvc.General.InvalidEmail")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string EmailConfirmation { get; set; }


        [DataType(DataType.Password)]
        [DisplayName("DancingGoatMvc.SignIn.Password")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [DisplayName("DancingGoatMvc.Register.PasswordConfirmation")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        [Compare("Password", ErrorMessage = "DancingGoatMvc.Register.PasswordConfirmation.Invalid")]
        public string PasswordConfirmation { get; set; }


        [DisplayName("DancingGoatMvc.FirstName")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string FirstName { get; set; }


        [DisplayName("DancingGoatMvc.LastName")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string LastName { get; set; }

        [DisplayName("DancingGoatMvc.Telephone")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string Telephone { get; set; }

        [DisplayName("DancingGoatMvc.LiveLocation")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string LiveLocation { get; set; }

        [DisplayName("DancingGoatMvc.WorkLocation")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        public string WorkLocation { get; set; }

        [DisplayName("DancingGoatMvc.Register.AnnualHouseholdIncome.Empty")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(10, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public string AnnualHouseholdIncome { get; set; }

        [DisplayName("DancingGoatMvc.Register.Savings")]
        [Required(ErrorMessage = "Required")]
        [MaxLength(10, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public string Savings { get; set; }

        //[DisplayName("DancingGoatMvc.Register.Savings")]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.Savings.Empty")]
        //[MaxLength(10, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //[DataType(DataType.Currency)]
        //[DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public string HearAboutUs { get; set; }


        public DateTime LeadDate { get; set; }
        public int LeadTypeId { get; set; }
        public int LeadSourceId { get; set; }
        public int MarketingSourceId { get; set; }
        public bool ParkingRequired { get; set; }
        public bool GardenRequired { get; set; }
        public bool OutdoorSpaceRequired { get; set; }
        public List<SchemesOfInterest> SchemesOfInterest { get; set; }
        public List<PropertyTypes> PropertyTypes { get; set; }
        public string BoroughsInterestedIn { get; set; }
        public int BedsRequired { get; set; }
        public int HomeBorough { get; set; }
        public int WorkBorough { get; set; }
        public bool MarketingMaterial { get; set; }
        public bool Newsletter { get; set; }
        public bool PrivacyPolicy { get; set; }
        public bool CanEmail { get; set; }
        public bool CanText { get; set; }



        //[DataType(DataType.EmailAddress)]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.EmailUserName.Empty")]
        //[DisplayName("DancingGoatMvc.Register.EmailUserName")]
        //[EmailAddress(ErrorMessage = "DancingGoatMvc.General.InvalidEmail")]
        //[MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //public string UserName { get; set; }


        //[DataType(DataType.Password)]
        //[DisplayName("DancingGoatMvc.Register.Password")]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.Password.Empty")]
        //[MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //public string Password { get; set; }


        //[DataType(DataType.Password)]
        //[DisplayName("DancingGoatMvc.Register.PasswordConfirmation")]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.PasswordConfirmation.Empty")]
        //[MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //[Compare("Password", ErrorMessage = "DancingGoatMvc.Register.PasswordConfirmation.Invalid")]
        //public string PasswordConfirmation { get; set; }


        //[DisplayName("DancingGoatMvc.FirstName")]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.FirstName.Empty")]
        //[MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //public string FirstName { get; set; }


        //[DisplayName("DancingGoatMvc.LastName")]
        //[Required(ErrorMessage = "DancingGoatMvc.Register.LastName.Empty")]
        //[MaxLength(100, ErrorMessage = "DancingGoatMvc.General.MaximumInputLengthExceeded")]
        //public string LastName { get; set; }
    }

    public class PropertyTypes
    {
    }

    public class SchemesOfInterest
    {
    }
}