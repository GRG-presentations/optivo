﻿using System.Collections.Generic;
using DancingGoat.Models.References;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.ContactPage
{
    public class ContactPageViewModel
    {
        public string TeaserPath { get; set; }


        public string Title { get; set; }


        public string Text { get; set; }


        public static ContactPageViewModel GetViewModel(ContactPageSectionViewModel contact, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new ContactPageViewModel
            {
                TeaserPath = contact.BackgroundImagePath,
                Title = contact.Heading,
                Text = contact.Text
            };
        }
    }
}