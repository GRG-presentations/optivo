﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class ContactCardWidgetProperties : IWidgetProperties
    {
        public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Title")]
        public string Title { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 3, Label = "Value1")]
        public string Value1 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Text1")]
        public string Text1 { get; set; }
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Icon1$}", Order = 4)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg;.svg;")]
        public IEnumerable<MediaFilesSelectorItem> Icon1 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 6, Label = "Value2")]
        public string Value2 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 5, Label = "Text2")]
        public string Text2 { get; set; }
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Icon2$}", Order = 7)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg;.svg;")]
        public IEnumerable<MediaFilesSelectorItem> Icon2 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();
    }
}
