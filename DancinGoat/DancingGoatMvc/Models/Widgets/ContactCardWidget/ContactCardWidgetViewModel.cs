﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class ContactCardWidgetViewModel
    {
        public string Title { get; set; }
        public string Value1 { get; set; }
        public string Text1 { get; set; }
        public string Icon1 { get; set; }
        public string Value2 { get; set; }
        public string Text2 { get; set; }
        public string Icon2 { get; set; }
    }
}