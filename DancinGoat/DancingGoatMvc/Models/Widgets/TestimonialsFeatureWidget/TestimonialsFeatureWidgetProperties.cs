﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class TestimonialsFeatureWidgetProperties : IWidgetProperties
    {
        public const string MEDIA_LIBRARY_NAME = "optivo";
        /// <summary>
        /// Button text.
        /// </summary>
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Image$}", Order = 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        public IEnumerable<MediaFilesSelectorItem> ImageGuid { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$ProfileImage$}", Order = 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        public IEnumerable<MediaFilesSelectorItem> ImageGuid1 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Title")]
        public string Title { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "CompanyName")]
        public string CompanyName { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Text")]
        public string Text { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "CompanyAddress")]
        public string CompanyAddress { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Date")]
        public string Date { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Youtube Link")]
        public string youtubeLink { get; set; }
    }
}
