﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class TestimonialsFeatureWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        public string ImagePath { get; set; }
        public string ImagePath1 { get; set; }
        public string Title { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
        public string youtubeLink { get; set; }
    }
}