﻿using System;

namespace DancingGoat.Models.Widgets
{
    public class MediaFileViewModel
    {
        public string FileTitle { get; set; }

        public string DirectUrl { get; set; }

        public string PermanentUrl { get; set; }
    }
}