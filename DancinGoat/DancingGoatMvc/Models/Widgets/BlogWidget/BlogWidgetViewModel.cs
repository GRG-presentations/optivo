﻿using System.Collections.Generic;

using DancingGoat.Models.Blogs;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for Articles widget.
    /// </summary>
    public class BlogWidgetViewModel
    {
        /// <summary>
        /// Latest articles to display.
        /// </summary>
        public IEnumerable<BlogViewModel> Blogs { get; set; }


        /// <summary>
        /// Number of articles to show.
        /// </summary>
        public int Count { get; set; }
    }
}