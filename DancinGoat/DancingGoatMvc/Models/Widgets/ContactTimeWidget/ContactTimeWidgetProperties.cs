﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class ContactTimeWidgetProperties : IWidgetProperties
    {
        public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Icon1$}", Order = 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg;.svg;")]
        public IEnumerable<MediaFilesSelectorItem> Image { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Title")]
        public string Title { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Days1")]
        public string Days1 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 3, Label = "Day2")]
        public string Day2 { get; set; }
       

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 6, Label = "Day3")]
        public string Day3 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 7, Label = "Days Working Time")]
        public string Value1 { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 8, Label = "Except Day1 Working Time")]
        public string Value2 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 9, Label = "Except Day2 Working Time")]
        public string Value3 { get; set; }
    }
}
