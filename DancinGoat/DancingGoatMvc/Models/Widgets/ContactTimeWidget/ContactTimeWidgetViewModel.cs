﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class ContactTimeWidgetViewModel
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Days1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
    }
}