﻿namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeSpecificationWidgetViewModel
    {
        /// <summary>
        /// Text of the button.
        /// </summary>
        public string Title { get; set; }
        public string Text { get; set; }
    }
}