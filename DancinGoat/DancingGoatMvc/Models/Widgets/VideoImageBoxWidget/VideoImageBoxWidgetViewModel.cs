﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class VideoImageBoxWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        /// 
        public string text { get; set; }
        public string buttonLink { get; set; }
        public string buttonText { get; set; }
        public string youtubeLink { get; set; }
        public string ImagePath { get; set; }
    }
}