﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class VideoImageBoxWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        /// public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Text")]
        public string text { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "ButtonLink")]
        public string buttonLink { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 3, Label = "ButtonText")]
        public string ButtonText { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 4, Label = "YoutubeLink")]
        public string YoutubeLink { get; set; }
        public Guid ImageGuid { get; set; }
    }
}
