﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class SlideshowWidgetProperties : IWidgetProperties
    {
        public Guid[] ImageGuids { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "{$Widget.MediaLibraryName$}", Order = 0)]
        public string MediaLibraryName { get; set; }
    }
}
