﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class HomeRegisterWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        /// 
        public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Title")]
        public string Title { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "CompanyName")]
        public string CompanyName { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "CompanyAddress")]
        public string CompanyAddress { get; set; }

        /// <summary>
        /// Guid of background image.
        /// </summary>
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Image", Order = 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> Image { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();
        /// <summary>
        /// Page where the button points to.
        /// </summary>
        [EditingComponent(UrlSelector.IDENTIFIER, Order = 1, Label = "LinkUrl")]
        [EditingComponentProperty(nameof(UrlSelectorProperties.Placeholder), "{$DancingGoatMVC.Widget.HomeDetails.LinkUrl.Placeholder$}")]
        [EditingComponentProperty(nameof(UrlSelectorProperties.Tabs), ContentSelectorTabs.Page)]
        public string LinkUrl { get; set; }


        /// <summary>
        /// Indicates if link should be opened in a new tab.
        /// </summary>
        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 2, Label = "OpenInNewTab")]
        public bool OpenInNewTab { get; set; }
        public string Text { get; set; }
    }
}
