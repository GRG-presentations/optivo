﻿namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeRegisterWidgetViewModel
    {
        /// <summary>
        /// Text of the button.
        /// </summary>
        public string Title { get; set; }
        public string Image { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Text { get; set; }

        public string ImagePath { get; set; }


        /// <summary>
        /// URL where the button points to.
        /// </summary>
        public string Url { get; set; }


        /// <summary>
        /// Indicates if link should be opened in a new tab.
        /// </summary>
        public bool OpenInNewTab { get; set; }
    }
}