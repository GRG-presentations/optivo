﻿using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.PageTemplates;
using DancingGoat.Models.Testimonials;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class Card
    {
        public int? minBedrooms { get; set; }
        public double? pricesFrom { get; set; }
        public int? shareFrom30 { get; set; }
        public string? minDespositFrom { get; set; }
        public string? totalMonthlyCostFrom { get; set; }
        public string? monthlyRentFrom { get; set; }
        public int? tenureTypeId { get; set; }
        public int? propertyId { get; set; }
        public bool FeaturedProperty { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public string Image { get; set; }
        public string DevelopmentId { get; set; }
        public string DevelopmentTitle { get; set; }
        public string DevelopmentLocation { get; set; }
    }
    public class FeaturedWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        public string ImagePath { get; set; }
        public List<PlotsViewModel> PlotData { get; set; }
        public List<Card> fullListing { get; set; }

        public static FeaturedWidgetViewModel GetViewModel(IEnumerable<PlotsViewModel> Plots, IEnumerable<ListViewModel> full)
        {
            var FeaturedList = new List<PlotsViewModel>();
            foreach (var item in Plots)
            {
                if (item.FeaturedProperty)
                {
                    FeaturedList.Add(item);
                }
            }
            var FeaturedList2 = new List<Card>();
            foreach (var item in full)
            {
                foreach (var item2 in item.CardData)
                {
                    if (item2.FeaturedProperty)
                    {
                        FeaturedList2.Add(new Card {
                            minBedrooms= item2.minBedrooms,
        pricesFrom= item2.pricesFrom,
        shareFrom30= item2.shareFrom30,
        minDespositFrom = item2.minDespositFrom,
        totalMonthlyCostFrom = item2.totalMonthlyCostFrom,
        monthlyRentFrom = item2.monthlyRentFrom,
        tenureTypeId = item2.tenureTypeId,
        propertyId= item2.propertyId,
        FeaturedProperty = item2.FeaturedProperty,
        Tag1 = item2.Tag1,
        Tag2 = item2.Tag2,
        Image = item2.Image,
        DevelopmentId = item.DevelopmentId,
        DevelopmentTitle = item.Title,
                            DevelopmentLocation= item.Location
                        });
                    }
                }
            }
            return new FeaturedWidgetViewModel
            {
                PlotData = FeaturedList,
                fullListing = FeaturedList2
            };
        }
    }
}