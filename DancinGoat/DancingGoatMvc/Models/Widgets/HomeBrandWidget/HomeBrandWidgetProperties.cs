﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class HomeBrandWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        /// 
        public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Title")]
        public string Title { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "ColorRGBA")]
        public string ColorRGBA { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 3, Label = "Text")]
        public string Text { get; set; }

        /// <summary>
        /// Guid of background image.
        /// </summary>
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Image", Order = 4)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> Image { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();
        /// <summary>
        /// Page where the button points to.
        /// </summary>


        /// <summary>
        /// Indicates if link should be opened in a new tab.
        /// </summary>
        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 5, Label = "Overview")]
        public bool Overview { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 6, Label = "Gallery")]
        public bool Gallery { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 7, Label = "Location")]
        public bool Location { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 8, Label = "Specification")]
        public bool Specification { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 9, Label = "Availability")]
        public bool Availability { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 10, Label = "DownloadBrochure")]
        public bool DownloadBrochure { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 11, Label = "Booking")]
        public bool Booking { get; set; }
    }
}
