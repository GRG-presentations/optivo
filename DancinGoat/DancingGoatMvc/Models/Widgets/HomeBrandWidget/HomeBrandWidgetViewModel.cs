﻿namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeBrandWidgetViewModel
    {
        /// <summary>
        /// Text of the button.
        /// </summary>
        public string Title { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string ColorRGBA { get; set; }
        public string Text { get; set; }

        public bool Overview { get; set; }
        public bool Gallery { get; set; }
        public bool Location { get; set; }
        public bool Specification { get; set; }
        public bool Availability { get; set; }
        public bool DownloadBrochure { get; set; }
        public bool Booking { get; set; }
    }
}