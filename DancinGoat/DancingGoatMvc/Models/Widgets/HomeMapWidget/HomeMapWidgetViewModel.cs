﻿namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeMapWidgetViewModel
    {
        /// <summary>
        /// Text of the button.
        /// </summary>
        public string Text { get; set; }


        /// <summary>
        /// URL where the button points to.
        /// </summary>
        public string Lan { get; set; }


        /// <summary>
        /// Indicates if link should be opened in a new tab.
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// URL where the button points to.
        /// </summary>
        public string MarkerLan { get; set; }


        /// <summary>
        /// Indicates if link should be opened in a new tab.
        /// </summary>
        public string MarkerLat { get; set; }
    }
}