﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class HomeMapWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Text")]
        public string Text { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Latitude")]
        public string Lan { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 3, Label = "Longitude")]
        public string Lat { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 4, Label = "MarkerLatitude")]
        public string MarkerLan { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 5, Label = "MarkerLongitude")]
        public string MarkerLat { get; set; }
    }
}
