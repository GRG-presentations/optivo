﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeGalleryWidgetViewModel
    {
        public IEnumerable<MediaFileViewModel> Images { get; set; }
        public IEnumerable<MediaFileViewModel> Videos { get; set; }
    }
}