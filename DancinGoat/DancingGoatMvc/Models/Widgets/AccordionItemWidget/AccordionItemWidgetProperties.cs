﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class AccordionItemWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Title")]
        public string Title { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Text")]
        public string Text { get; set; }
    }
}
