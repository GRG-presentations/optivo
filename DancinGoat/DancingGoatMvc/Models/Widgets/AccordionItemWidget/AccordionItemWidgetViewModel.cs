﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class AccordionItemWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        public string Title { get; set; }
        public string Text { get; set; }
    }
}