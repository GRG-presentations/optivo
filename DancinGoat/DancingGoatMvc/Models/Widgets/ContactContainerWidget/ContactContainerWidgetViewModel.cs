﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class ContactContainerWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        public string ImagePath { get; set; }
    }
}