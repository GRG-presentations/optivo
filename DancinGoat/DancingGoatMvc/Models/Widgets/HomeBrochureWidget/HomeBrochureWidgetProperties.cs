﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class HomeBrochureWidgetProperties : IWidgetProperties
    {
        /// <summary>
        /// Button text.
        /// </summary>
        /// 
        public const string MEDIA_LIBRARY_NAME = "optivo";
        public string Title { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "ColorRGBA")]
        public string ColorRGBA { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 5, Label = "Development Brochure")]
        public bool DevelopmentBrochure { get; set; }

        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Development Brochure File", Order = 6)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".pdf;")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> File1 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 7, Label = "Shared Ownership Floorplans")]
        public bool SharedOwnershipFloorplans { get; set; }

        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Shared Ownership Floorplan File", Order = 8)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".pdf;")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> File2 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 9, Label = "Private Sale floorplans")]
        public bool PrivateSalefloorplans { get; set; }

        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Private Sale Floorplan File", Order = 10)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".pdf;")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> File3 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 11, Label = "Rent to Buy Floorplans")]
        public bool RentToBuyFloorplans { get; set; }

        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "Rent to Buy Floorplan File", Order = 12)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".pdf;")]
        [Required]
        public IEnumerable<MediaFilesSelectorItem> File4 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();
        public string Text { get; set; }
    }
}
