﻿namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class HomeBrochureWidgetViewModel
    {
        /// <summary>
        /// Text of the button.
        /// </summary>
        public string Title { get; set; }
        public string Text { get; set; }
        public string ColorRGBA { get; set; }

        public bool DevelopmentBrochure { get; set; }
        public string DownloadDevelopmentBrochure { get; set; }
        public bool SharedOwnershipFloorplans { get; set; }
        public string DownloadSharedOwnershipFloorplans { get; set; }
        public bool PrivateSaleFloorplans { get; set; }
        public string DownloadPrivateSaleFloorplans { get; set; }
        public bool RentToBuyFloorplans { get; set; }
        public string DownloadRentToBuyFloorplans { get; set; }

    }
}