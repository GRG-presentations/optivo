﻿using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class MoreFromOptivoWidgetViewModel
    {
        public string Title1 { get; set; }
        public string Text1 { get; set; }
        public string Image1 { get; set; }
        public string ButtonText1 { get; set; }
        public string ButtonLink1 { get; set; }
        public string Title2 { get; set; }
        public string Text2 { get; set; }
        public string Image2 { get; set; }
        public string ButtonText2 { get; set; }
        public string ButtonLink2 { get; set; }
    }
}