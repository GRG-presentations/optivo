﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// CTA button widget properties.
    /// </summary>
    public class MoreFromOptivoWidgetProperties : IWidgetProperties
    {
        public const string MEDIA_LIBRARY_NAME = "optivo";
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "Title1")]
        public string Title1 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 2, Label = "Text1")]
        public string Text1 { get; set; }
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Image1$}", Order = 3)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        public IEnumerable<MediaFilesSelectorItem> Image1 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 4, Label = "ButtonText1")]
        public string ButtonText1 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 5, Label = "ButtonLink1")]
        public string ButtonLink1 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 6, Label = "Title2")]
        public string Title2 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 7, Label = "Text2")]
        public string Text2 { get; set; }
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Label = "{$Image2$}", Order = 8)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.LibraryName), MEDIA_LIBRARY_NAME)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.MaxFilesLimit), 1)]
        [EditingComponentProperty(nameof(MediaFilesSelectorProperties.AllowedExtensions), ".gif;.png;.jpg;.jpeg")]
        public IEnumerable<MediaFilesSelectorItem> Image2 { get; set; } = Enumerable.Empty<MediaFilesSelectorItem>();

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 9, Label = "ButtonText2")]
        public string ButtonText2 { get; set; }
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 10, Label = "ButtonLink2")]
        public string ButtonLink2 { get; set; }

    }
}
