﻿using DancingGoat.Models.Testimonials;
using System.Collections.Generic;

namespace DancingGoat.Models.Widgets
{
    /// <summary>
    /// View model for the CTA Button widget.
    /// </summary>
    public class TestimonialsListWidgetViewModel
    {
        /// <summary>
        /// Image.
        /// </summary>
        public string ImagePath { get; set; }
        public IEnumerable<TestimonialViewModel> Testimonials { get; set; }
    }
}