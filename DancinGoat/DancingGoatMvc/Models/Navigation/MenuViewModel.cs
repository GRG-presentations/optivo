﻿using System.Collections.Generic;

namespace DancingGoat.Models.Navigation
{
    public class MenuViewModel
    {
        public IEnumerable<MenuItemModel> Items { get; set; }
        public IEnumerable<MenuItemModel> propertiesItems { get; set; }
    }
}