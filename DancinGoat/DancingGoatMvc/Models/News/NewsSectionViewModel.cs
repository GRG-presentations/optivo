﻿using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.News
{
    public class NewsSectionViewModel
    {
        public string Heading { get; set; }


        public string Text { get; set; }


        public string ImagePath { get; set; }


        public static NewsSectionViewModel GetViewModel(NewsSection newsSection, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new NewsSectionViewModel
            {
                Heading = newsSection.Fields.NewsHeading,
                Text = newsSection.Fields.Text,
                ImagePath = newsSection.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(newsSection.Fields.Image).RelativePath
            };
        }
    }
}