﻿using System.Collections.Generic;
using CMS.Taxonomy;
using DancingGoat.Models.Blogs;
using DancingGoat.Models.References;

namespace DancingGoat.Models.News
{
    public class NewsViewModel
    {
        public IEnumerable<NewsSectionViewModel> Sections { get; set; }

        public ReferenceViewModel Reference {get; set;}
        public IEnumerable<BlogViewModel> Blogs {get; set; }
        public List<CategoriesViewModel> Categories {get; set; }
    }
}