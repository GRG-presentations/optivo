﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using CMS.Taxonomy;
using DancingGoat.Models.PageTemplates;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.Blogs
{
    public class Category {
        public string CategoryDisplayName { get; set; }
    }
    public class BlogViewModel
    {
        public IPageAttachmentUrl TeaserUrl { get; set; }


        public string Title { get; set; }


        public DateTime PublicationDate { get; set; }


        public string Summary { get; set; }


        public string Text { get; set; }


        public IEnumerable<TeamMemberViewModel> Author { get; set; }
        public List<CategoryInfo> Categories { get; set; }


        public string Url { get; set; }


        public static BlogViewModel GetViewModel(Blog blog, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            List<CategoryInfo> CategoriesList = new List<CategoryInfo> { };
            if (blog.Categories.Count > 0)
            {
                foreach(var item in blog.Categories)
                {
                    CategoriesList.Add((CategoryInfo)item);
                }
            }
            return new BlogViewModel
            {
                PublicationDate = blog.PublicationDate,
                Author = blog.Fields.Author.OfType<TeamMember>().Select(author => TeamMemberViewModel.GetViewModel(author, true, pageUrlRetriever, attachmentUrlRetriever)),
                Summary = blog.Fields.Summary,
                TeaserUrl = blog.Fields.Teaser == null ? null : attachmentUrlRetriever.Retrieve(blog.Fields.Teaser),
                Text = blog.Fields.Text,
                Title = blog.Fields.Title,
                Url = pageUrlRetriever.Retrieve(blog).RelativePath,
                Categories = CategoriesList
            };
        }
    }
}