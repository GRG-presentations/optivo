﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using CMS.Taxonomy;
using DancingGoat.Models.PageTemplates;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.Blogs
{
    public class CategoriesViewModel
    {


        public int Id { get; set; }
        public int Total { get; set; }


        public string Name { get; set; }


        public static CategoriesViewModel GetViewModel(CategoryInfo cat, IEnumerable<BlogViewModel> Blogs)
        {
            int numOfItems = 0;
            foreach(var item in Blogs)
            {
                foreach (var categ in item.Categories)
                {
                    if(categ.CategoryID == cat.CategoryID)
                    {
                        numOfItems++;
                    }
                }
            }
            return new CategoriesViewModel
            {
                Id = cat.CategoryID,
                Name = cat.CategoryDisplayName,
                Total = numOfItems
            };
        }
    }
}