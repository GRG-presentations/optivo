﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.PageTemplates;
using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.Testimonials
{
    public class TestimonialViewModel
    {
        public IPageAttachmentUrl TeaserUrl { get; set; }


        public string Title { get; set; }


        public DateTime PublicationDate { get; set; }


        public string Summary { get; set; }


        public string Text { get; set; }


        public string Address { get; set; }


        public string Name { get; set; }




        public IEnumerable<TeamMemberViewModel> Author { get; set; }
        
        
        public string Url { get; set; }


        public static TestimonialViewModel GetViewModel(Testimonial testimonial, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new TestimonialViewModel
            {
                PublicationDate = testimonial.PublicationDate,
                Author = testimonial.Fields.Author.OfType<TeamMember>().Select(author => TeamMemberViewModel.GetViewModel(author,true, pageUrlRetriever, attachmentUrlRetriever)),
                Summary = testimonial.Fields.Summary,
                TeaserUrl = testimonial.Fields.Teaser == null ? null : attachmentUrlRetriever.Retrieve(testimonial.Fields.Teaser),
                Text = testimonial.Fields.Text,
                Title = testimonial.Fields.Title,
                Address = testimonial.Fields.Address,
                Name = testimonial.Fields.Name,
                Url = pageUrlRetriever.Retrieve(testimonial).RelativePath
            };
        }
    }
}