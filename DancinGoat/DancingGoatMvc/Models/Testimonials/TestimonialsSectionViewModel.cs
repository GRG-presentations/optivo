﻿using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Models.Testimonials
{
    public class TestimonialsSectionViewModel
    {
        public string Heading { get; set; }


        public string Text { get; set; }


        public string ImagePath { get; set; }


        public static TestimonialsSectionViewModel GetViewModel(TestimonialsSection testimonialsSection, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            return new TestimonialsSectionViewModel
            {
                Heading = testimonialsSection.Fields.Heading,
                Text = testimonialsSection.Fields.Text,
                ImagePath = testimonialsSection.Fields.Image == null ? null : attachmentUrlRetriever.Retrieve(testimonialsSection.Fields.Image).RelativePath
            };
        }
    }
}