﻿using System.Collections.Generic;
using DancingGoat.Models.Testimonials;
using DancingGoat.Models.References;

namespace DancingGoat.Models.Testimonials
{
    public class TestimonialsViewModel
    {
        public IEnumerable<TestimonialsSectionViewModel> Sections { get; set; }

        public ReferenceViewModel Reference {get; set;}
        public IEnumerable<TestimonialViewModel> Testimonials {get; set; }
    }
}