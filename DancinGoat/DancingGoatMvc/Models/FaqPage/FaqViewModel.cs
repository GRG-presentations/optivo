﻿using System.Collections.Generic;
using DancingGoat.Models.References;

namespace DancingGoat.Models.FaqPage
{
    public class FaqViewModel
    {
        public IEnumerable<FaqSectionViewModel> FaqSections { get; set; }

        public ReferenceViewModel Reference { get; set; }
    }
}