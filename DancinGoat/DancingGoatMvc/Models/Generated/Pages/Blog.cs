﻿using System;

namespace CMS.DocumentEngine.Types.DancingGoatMvc
{
    /// <summary>
    /// Custom Blog members.
    /// </summary>
    public partial class Blog
    {
        public DateTime PublicationDate
        {
            get
            {
                return GetDateTimeValue("DocumentPublishFrom", GetDateTimeValue("DocumentCreatedWhen", DateTime.MinValue));
            }
        }
    }
}