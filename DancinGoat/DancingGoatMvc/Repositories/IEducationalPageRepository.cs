﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using CMS.DocumentEngine.Types.DancingGoatMvc;

namespace DancingGoat.Repositories
{
    /// <summary>
    /// Represents a contract for a collection of educational page sections.
    /// </summary>
    public interface IEducationalPageRepository : IRepository
    {
        /// <summary>
        /// Asynchronously returns an enumerable collection of educational page sections ordered by a position in the content tree.
        /// </summary>
        /// <param name="nodeAliasPath">The node alias path of the educational in the content tree.</param>
        /// <param name="cancellationToken">The cancellation instruction.</param>
        Task<IEnumerable<EducationalPageSection>> GetEducationalPageSectionsAsync(string nodeAliasPath, CancellationToken cancellationToken);

        IEnumerable<EducationalPage> GetEducationalPages(string nodeAliasPath, int count = 0);


        /// <summary>
        /// Returns an enumerable collection of educational page sections ordered by a position in the content tree.
        /// </summary>
        /// <param name="nodeAliasPath">The node alias path of the educational in the content tree.</param>
        IEnumerable<EducationalPageSection> GetEducationalPageSections(string nodeAliasPath);

    }
}