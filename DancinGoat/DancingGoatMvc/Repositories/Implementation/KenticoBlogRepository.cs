﻿using System.Collections.Generic;
using System.Linq;

using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;

namespace DancingGoat.Repositories.Implementation
{
    /// <summary>
    /// Represents a collection of articles.
    /// </summary>
    public class KenticoBlogRepository : IBlogRepository
    {
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IPageRetriever pageRetriever;


        /// <summary>
        /// Initializes a new instance of the <see cref="KenticoBlogRepository"/> class that returns articles.
        /// </summary>
        /// <param name="pageRetriever">Retriever for pages based on given parameters.</param>
        /// <param name="pageDataContextRetriever">Retriever for page data context.</param>
        public KenticoBlogRepository(IPageRetriever pageRetriever, IPageDataContextRetriever pageDataContextRetriever)
        {
            this.pageRetriever = pageRetriever;
            this.pageDataContextRetriever = pageDataContextRetriever;
        }


        /// <summary>
        /// Returns an enumerable collection of articles ordered by the date of publication. The most recent articles come first.
        /// </summary>
        /// <param name="nodeAliasPath">The node alias path of the articles section in the content tree.</param>
        /// <param name="count">The number of articles to return. Use 0 as value to return all records.</param>
        public IEnumerable<Blog> GetBlogs(string nodeAliasPath, int count = 0)
        {
            return pageRetriever.Retrieve<Blog>(
                query => query
                    .Path(nodeAliasPath, PathTypeEnum.Children)
                    .TopN(count)
                    .OrderByDescending("DocumentPublishFrom"),
                cache => cache
                    .Key($"{nameof(KenticoBlogRepository)}|{nameof(GetBlogs)}|{nodeAliasPath}|{count}")
                    // Include path dependency to flush cache when a new child page is created.
                    .Dependencies((_, builder) => builder.PagePath(nodeAliasPath, PathTypeEnum.Children)));
        }


        /// <summary>
        /// Returns the current article based on the current page data context.
        /// </summary>
        public Blog GetCurrent()
        {
            var page = pageDataContextRetriever.Retrieve<Blog>().Page;

            return pageRetriever.Retrieve<Blog>(
                query => query
                    .WhereEquals("NodeID", page.NodeID),
                cache => cache
                    .Key($"{nameof(KenticoBlogRepository)}|{nameof(GetCurrent)}|{page.NodeID}")
                    .Dependencies((blogs, deps) => deps.Pages(blogs.First().Fields.Author)))
                .First();
        }
    }
}