﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;

using DancingGoat.Controllers;
using DancingGoat.Infrastructure;
using DancingGoat.Models.News;
using DancingGoat.Models.References;
using DancingGoat.Repositories;
using CMS.DocumentEngine;
using DancingGoat.Models.Blogs;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;
using CMS.Taxonomy;
using System.Collections.Generic;

[assembly: RegisterPageRoute(News.CLASS_NAME, typeof(NewsController))]
[assembly: RegisterPageRoute(News.CLASS_NAME, typeof(NewsController), ActionName = "Show")]

namespace DancingGoat.Controllers
{
    public class Categories
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class NewsController : Controller
    {
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageDataContextRetriever dataRetriever;
        private readonly INewsRepository newsRepository;
        private readonly IBlogRepository blogRepository;
        private readonly IReferenceRepository referenceRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;

        public NewsController(IPageDataContextRetriever dataRetriever,
            INewsRepository newsRepository,
            IBlogRepository blogRepository,
            IPageUrlRetriever pageUrlRetriever,
            IReferenceRepository referenceRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.dataRetriever = dataRetriever;
            this.newsRepository = newsRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.blogRepository = blogRepository;
            this.referenceRepository = referenceRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        [OutputCache(CacheProfile = "Default")]
        // GET: News
        public async Task<ActionResult> Index(CancellationToken cancellationToken)
        {
            var news = dataRetriever.Retrieve<News>().Page;
            var category = CategoryInfoProvider.GetCategories();
            var sideStories = await newsRepository.GetSideStoriesAsync(news.NodeAliasPath, cancellationToken);
            /*outputCacheDependencies.AddDependencyOnPages(sideStories);*/

            var reference = (await referenceRepository.GetReferencesAsync($"{news.NodeAliasPath}/References", cancellationToken, 1)).FirstOrDefault();
            /*outputCacheDependencies.AddDependencyOnPage(reference);*/
            var section = dataRetriever.Retrieve<TreeNode>().Page;
            var blogs = blogRepository.GetBlogs("/Blogs");
            IEnumerable<BlogViewModel> BlogProt = blogs.Select(blog => BlogViewModel.GetViewModel(blog, pageUrlRetriever, attachmentUrlRetriever));

            NewsViewModel mode = new NewsViewModel()
            {
                Sections = sideStories.Select(story => NewsSectionViewModel.GetViewModel(story, attachmentUrlRetriever)),
                Reference = ReferenceViewModel.GetViewModel(reference, attachmentUrlRetriever),
                Blogs = BlogProt,
                Categories = category.Select(cat => CategoriesViewModel.GetViewModel(cat, BlogProt)).ToList()
            };

            return View(mode);
        }
        // GET: Blogs
        [OutputCache(CacheProfile = "Default")]
        public ActionResult Show()
        {
            var blog = blogRepository.GetCurrent();

            outputCacheDependencies.AddDependencyOnPage(blog);

            return new TemplateResult(blog);
        }
    }
}