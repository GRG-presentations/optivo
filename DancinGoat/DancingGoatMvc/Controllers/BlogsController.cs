﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

using DancingGoat.Controllers;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Blogs;
using DancingGoat.Repositories;

[assembly: RegisterPageRoute(BlogSection.CLASS_NAME, typeof(BlogsController))]
[assembly: RegisterPageRoute(Blog.CLASS_NAME, typeof(BlogsController), ActionName = "Show")]

namespace DancingGoat.Controllers
{
    public class BlogsController : Controller
    {
        private readonly IPageDataContextRetriever dataContextRetriever;
        private readonly IBlogRepository blogRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;
        private readonly IOutputCacheDependencies outputCacheDependencies;

        public BlogsController(IPageDataContextRetriever dataContextRetriever,
            IBlogRepository blogRepository,
            IPageUrlRetriever pageUrlRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever,
            IOutputCacheDependencies outputCacheDependencies)
        {
            this.dataContextRetriever = dataContextRetriever;
            this.blogRepository = blogRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
        }
        // GET: Blogs
        [OutputCache(CacheProfile = "Default")]
        public ActionResult Blogs()
        {
            var section = dataContextRetriever.Retrieve<TreeNode>().Page;
            var blogs = blogRepository.GetBlogs(section.NodeAliasPath);

            outputCacheDependencies.AddDependencyOnPages(blogs);

            return PartialView(blogs.Select(blog => BlogViewModel.GetViewModel(blog, pageUrlRetriever, attachmentUrlRetriever)));
        }

        // GET: Blogs
        [OutputCache(CacheProfile = "Default")]
        public ActionResult Show()
        {
            var blog = blogRepository.GetCurrent();

            outputCacheDependencies.AddDependencyOnPage(blog);

            return new TemplateResult(blog);
        }
    }
}