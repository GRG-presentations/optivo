﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;

using DancingGoat.Controllers;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Testimonials;
using DancingGoat.Models.References;
using DancingGoat.Repositories;
using CMS.DocumentEngine;
using DancingGoat.Models.Blogs;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageRoute(Testimonials.CLASS_NAME, typeof(TestimonialsController))]
[assembly: RegisterPageRoute(Testimonials.CLASS_NAME, typeof(TestimonialsController), ActionName = "Show")]

namespace DancingGoat.Controllers
{
    public class TestimonialsController : Controller
    {
        private readonly IPageDataContextRetriever dataContextRetriever;
        private readonly IPageDataContextRetriever dataRetriever;
        private readonly ITestimonialsRepository testimonialsRepository;
        private readonly ITestimonialRepository testimonialRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;
        private readonly IOutputCacheDependencies outputCacheDependencies;


        public TestimonialsController(IPageDataContextRetriever dataContextRetriever,
            ITestimonialsRepository testimonialsRepository,
            ITestimonialRepository testimonialRepository,
            IPageDataContextRetriever dataRetriever,
            IPageUrlRetriever pageUrlRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever,
            IOutputCacheDependencies outputCacheDependencies)
        {
            this.dataContextRetriever = dataContextRetriever;
            this.testimonialsRepository = testimonialsRepository;
            this.testimonialRepository = testimonialRepository;
            this.dataRetriever = dataRetriever;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
        }
        // GET: Testimonials
        [OutputCache(CacheProfile = "Default")]
        public async Task<ActionResult> Index(CancellationToken cancellationToken)
        {
            var testimonials = dataRetriever.Retrieve<Testimonials>().Page;

            var sideStories = await testimonialsRepository.GetSideStoriesAsync(testimonials.NodeAliasPath, cancellationToken);
            outputCacheDependencies.AddDependencyOnPages(sideStories);
            var testimonial = testimonialRepository.GetTestimonials("/Testimonials/Testimonials-List");

            /*var reference = (await referenceRepository.GetReferencesAsync($"{aboutUs.NodeAliasPath}/References", cancellationToken, 1)).FirstOrDefault();
            outputCacheDependencies.AddDependencyOnPage(reference);*/

            TestimonialsViewModel mode = new TestimonialsViewModel()
            {
                Sections = sideStories.Select(story => TestimonialsSectionViewModel.GetViewModel(story, attachmentUrlRetriever)),
                /*Reference = ReferenceViewModel.GetViewModel(reference, attachmentUrlRetriever)*/
                Testimonials = testimonial.Select(testimonial => TestimonialViewModel.GetViewModel(testimonial, pageUrlRetriever, attachmentUrlRetriever))
            };

            return View(mode);
        }
    }
}