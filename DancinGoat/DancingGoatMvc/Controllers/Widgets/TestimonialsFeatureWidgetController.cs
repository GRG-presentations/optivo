﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(TestimonialsFeatureWidgetController.IDENTIFIER, typeof(TestimonialsFeatureWidgetController), "{$TestimonialsFeature$}", Description = "{$dancinggoatmvc.widget.testimonialsfeature.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class TestimonialsFeatureWidgetController : WidgetController<TestimonialsFeatureWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.TestimonialsFeatureWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public TestimonialsFeatureWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.mediaFileRepository = mediaFileRepository;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<TestimonialsFeatureWidgetProperties>();
            var image = GetImage(properties);
            var image1 = GetImageProfile(properties);

            outputCacheDependencies.AddDependencyOnInfoObject<MediaFileInfo>(image?.FileGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_TestimonialsFeatureWidget", new TestimonialsFeatureWidgetViewModel
            {
                ImagePath = image == null ? null : mediaFileUrlRetriever.Retrieve(image).RelativePath,
                ImagePath1 = image1 == null ? null : mediaFileUrlRetriever.Retrieve(image1).RelativePath,
                Title = properties.Title,
                CompanyName = properties.CompanyName,
                CompanyAddress = properties.CompanyAddress,
                Text = properties.Text,
                Date = properties.Date,
                youtubeLink = properties.youtubeLink
            });
        }
        private MediaFileInfo GetImage(TestimonialsFeatureWidgetProperties properties)
        {
            var imageGuid = properties.ImageGuid.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
        private MediaFileInfo GetImageProfile(TestimonialsFeatureWidgetProperties properties)
        {
            var imageGuid = properties.ImageGuid1.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}