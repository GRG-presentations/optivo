﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;
using DancingGoat.Models.Testimonials;

[assembly: RegisterWidget(TestimonialsListWidgetController.IDENTIFIER, typeof(TestimonialsListWidgetController), "{$TestimonialsList$}", Description = "{$dancinggoatmvc.widget.testimonialslist.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class TestimonialsListWidgetController : WidgetController<TestimonialsListWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.TestimonialsListWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly ITestimonialRepository testimonialRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public TestimonialsListWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IOutputCacheDependencies outputCacheDependencies,
            IPageUrlRetriever pageUrlRetriever,
            ITestimonialRepository testimonialRepository,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.testimonialRepository = testimonialRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<TestimonialsListWidgetProperties>();
            var image = GetImage(properties);
            var testimonial = testimonialRepository.GetTestimonials("/Testimonials-List");

            outputCacheDependencies.AddDependencyOnPageAttachmnent(image?.AttachmentGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_TestimonialsListWidget", new TestimonialsListWidgetViewModel
            {
                ImagePath = image == null ? null : attachmentUrlRetriever.Retrieve(image).RelativePath,
                Testimonials = testimonial.Select(testimonial => TestimonialViewModel.GetViewModel(testimonial, pageUrlRetriever, attachmentUrlRetriever))
            });
        }
        private DocumentAttachment GetImage(TestimonialsListWidgetProperties properties)
        {
            var page = pageDataContextRetriever.Retrieve<TreeNode>().Page;
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}