﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;

[assembly: RegisterWidget(SlideshowWidgetController.IDENTIFIER, typeof(SlideshowWidgetController), "{$SlideShow$}", Description = "{$dancinggoatmvc.widget.slideshow.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class SlideshowWidgetController : WidgetController<SlideshowWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.SlideshowWidget";


        /*public IMediaLibraryRepository MediaLibraryRepository { get; }*/

        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        /*public SlideshowWidgetController(
            IMediaLibraryRepository mediaLibraryRepository)
        {
            MediaLibraryRepository = mediaLibraryRepository ?? throw new ArgumentNullException(nameof(mediaLibraryRepository));
            MediaLibraryRepository.MediaLibrarySiteName = "DancingGoatMvc";
        }*/
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = GetProperties();
            /*MediaLibraryRepository.MediaLibraryName = properties.MediaLibraryName;
            var images = MediaLibraryRepository.GetMediaLibraryDtos(properties.ImageGuids);*/

            return PartialView("Widgets/_DancingGoat_General_SlideshowWidget", new SlideshowWidgetViewModel
            {
                MediaLibraryViewModel = new Models.MediaLibraryViewModel
                {
                    LibraryName = properties.MediaLibraryName
                }
            });
        }
    }
}