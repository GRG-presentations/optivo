﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(AccordionContainerWidgetController.IDENTIFIER, typeof(AccordionContainerWidgetController), "{$AccordionContainer$}", Description = "{$dancinggoatmvc.widget.accordioncontainer.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class AccordionContainerWidgetController : WidgetController<AccordionContainerWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.AccordionContainerWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public AccordionContainerWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<AccordionContainerWidgetProperties>();

            return PartialView("Widgets/_DancingGoat_General_AccordionContainerWidget", new AccordionContainerWidgetViewModel
            {
                ImagePath = null
            });
        }
    }
}