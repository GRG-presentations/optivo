﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(ContactContainerWidgetController.IDENTIFIER, typeof(ContactContainerWidgetController), "{$ContactContainer$}", Description = "{$dancinggoatmvc.widget.contactcontainer.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class ContactContainerWidgetController : WidgetController<ContactContainerWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.ContactContainerWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public ContactContainerWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<ContactContainerWidgetProperties>();
            var image = GetImage(properties);

            outputCacheDependencies.AddDependencyOnPageAttachmnent(image?.AttachmentGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_ContactContainerWidget", new ContactContainerWidgetViewModel
            {
                ImagePath = image == null ? null : attachmentUrlRetriever.Retrieve(image).RelativePath
            });
        }
        private DocumentAttachment GetImage(ContactContainerWidgetProperties properties)
        {
            var page = pageDataContextRetriever.Retrieve<TreeNode>().Page;
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}