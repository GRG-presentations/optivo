﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(VideoImageWidgetController.IDENTIFIER, typeof(VideoImageWidgetController), "{$VideoImageBox$}", Description = "{$dancinggoatmvc.widget.videoimagebox.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class VideoImageWidgetController : WidgetController<VideoImageBoxWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.VideoImageBoxWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public VideoImageWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<VideoImageBoxWidgetProperties>();
            var image = GetImage(properties);

            outputCacheDependencies.AddDependencyOnPageAttachmnent(image?.AttachmentGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_VideoImageBoxWidget", new VideoImageBoxWidgetViewModel
            {
                ImagePath = image == null ? null : attachmentUrlRetriever.Retrieve(image).RelativePath,
                text = properties.text,
                buttonText = properties.ButtonText,
                buttonLink = properties.buttonLink,
                youtubeLink = properties.YoutubeLink
            });
        }
        private DocumentAttachment GetImage(VideoImageBoxWidgetProperties properties)
        {
            var page = pageDataContextRetriever.Retrieve<TreeNode>().Page;
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}