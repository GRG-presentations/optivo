﻿
using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Models.Widgets;

[assembly: RegisterWidget(AccordionItemWidgetController.IDENTIFIER, typeof(AccordionItemWidgetController), "{$AccordionItem$}", Description = "{$dancinggoatmvc.widget.accordionitem.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class AccordionItemWidgetController : WidgetController<AccordionItemWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.AccordionItemWidget";

        private readonly IComponentPropertiesRetriever propertiesRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public AccordionItemWidgetController(
            IComponentPropertiesRetriever propertiesRetriever)
        {
            this.propertiesRetriever = propertiesRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<AccordionItemWidgetProperties>();


            return PartialView("Widgets/_DancingGoat_General_AccordionItemWidget", new AccordionItemWidgetViewModel
            {
                Title = properties.Title,
                Text = properties.Text
            });
        }
    }
}