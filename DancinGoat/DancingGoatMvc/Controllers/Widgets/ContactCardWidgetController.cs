﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(ContactCardWidgetController.IDENTIFIER, typeof(ContactCardWidgetController), "{$ContactCard$}", Description = "{$dancinggoatmvc.widget.contactcard.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class ContactCardWidgetController : WidgetController<ContactCardWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.ContactCardWidget";

        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public ContactCardWidgetController(
            IMediaFileRepository mediaFileRepository,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<ContactCardWidgetProperties>();
            var icon1 = GetIcon1(properties);
            var icon2 = GetIcon2(properties);

            return PartialView("Widgets/_DancingGoat_General_ContactCardWidget", new ContactCardWidgetViewModel
            {
                Icon1 = icon1 == null ? null : mediaFileUrlRetriever.Retrieve(icon1).RelativePath,
                Title = properties.Title,
                Icon2 = icon2 == null ? null : mediaFileUrlRetriever.Retrieve(icon2).RelativePath,
                Text1 = properties.Text1,
                Text2 = properties.Text2,
                Value2 = properties.Value2,
                Value1 = properties.Value1
            });
        }
        private MediaFileInfo GetIcon1(ContactCardWidgetProperties properties)
        {
            var imageGuid = properties.Icon1.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
        private MediaFileInfo GetIcon2(ContactCardWidgetProperties properties)
        {
            var imageGuid = properties.Icon2.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}