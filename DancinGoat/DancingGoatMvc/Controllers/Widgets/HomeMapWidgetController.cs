﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;

[assembly: RegisterWidget(HomeMapWidgetController.IDENTIFIER, typeof(HomeMapWidgetController), "{$dancinggoatmvc.widget.homemap.name$}", Description = "{$dancinggoatmvc.widget.homemap.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class HomeMapWidgetController : WidgetController<HomeMapWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.HomeMapWidget";


        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public HomeMapWidgetController(
            IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<HomeMapWidgetProperties>();

            return PartialView("Widgets/_DancingGoat_General_HomeMapWidget", new HomeMapWidgetViewModel
            {
                Text = properties.Text,
                Lan = properties.Lan,
                Lat = properties.Lat,
                MarkerLan = properties.MarkerLan,
                MarkerLat = properties.MarkerLat
            });
        }
    }
}