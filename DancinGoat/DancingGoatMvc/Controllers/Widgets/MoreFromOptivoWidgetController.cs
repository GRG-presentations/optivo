﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(MoreFromOptivoWidgetController.IDENTIFIER, typeof(MoreFromOptivoWidgetController), "{$MoreFromOptivo$}", Description = "{$dancinggoatmvc.widget.morefromoptivo.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class MoreFromOptivoWidgetController : WidgetController<MoreFromOptivoWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.MoreFromOptivoWidget";

        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public MoreFromOptivoWidgetController(
           IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<MoreFromOptivoWidgetProperties>();
            var image1 = GetImage1(properties);
            var image2 = GetImage2(properties);


            return PartialView("Widgets/_DancingGoat_General_MoreFromOptivoWidget", new MoreFromOptivoWidgetViewModel
            {
                Title1 = properties.Title1,
                Title2 = properties.Title2,
                Text1 = properties.Text1,
                Text2 = properties.Text2,
                Image1 = image1 == null ? null : mediaFileUrlRetriever.Retrieve(image1).RelativePath,
                Image2 = image2 == null ? null : mediaFileUrlRetriever.Retrieve(image2).RelativePath,
                ButtonText1 = properties.ButtonText1,
                ButtonText2 = properties.ButtonText2,
                ButtonLink1 = properties.ButtonLink1,
                ButtonLink2 = properties.ButtonLink2
            });
        }
        private MediaFileInfo GetImage1(MoreFromOptivoWidgetProperties properties)
        {
            var imageGuid = properties.Image1.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
        private MediaFileInfo GetImage2(MoreFromOptivoWidgetProperties properties)
        {
            var imageGuid = properties.Image2.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}