﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;

[assembly: RegisterWidget(HomeSpecificationWidgetController.IDENTIFIER, typeof(HomeSpecificationWidgetController), "{$HomeSpecificationInfo$}", Description = "{$dancinggoatmvc.widget.homespecification.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class HomeSpecificationWidgetController : WidgetController<HomeSpecificationWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.HomeSpecificationWidget";
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public HomeSpecificationWidgetController(
            IComponentPropertiesRetriever componentPropertiesRetriever)
        {
            this.componentPropertiesRetriever = componentPropertiesRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<HomeSpecificationWidgetProperties>();

            return PartialView("Widgets/_DancingGoat_General_HomeSpecificationWidget", new HomeSpecificationWidgetViewModel
            {
                Title = properties.Title,
                Text = properties.Text
            });
        }
    }
}