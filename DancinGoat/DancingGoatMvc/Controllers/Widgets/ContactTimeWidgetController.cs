﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;

[assembly: RegisterWidget(ContactTimeWidgetController.IDENTIFIER, typeof(ContactTimeWidgetController), "{$ContactTime$}", Description = "{$dancinggoatmvc.widget.contacttime.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class ContactTimeWidgetController : WidgetController<ContactTimeWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.ContactTimeWidget";

        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public ContactTimeWidgetController(
           IMediaFileRepository mediaFileRepository,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<ContactTimeWidgetProperties>();
            var image = GetImage(properties);

            return PartialView("Widgets/_DancingGoat_General_ContactTimeWidget", new ContactTimeWidgetViewModel
            {
                Image = image == null ? null : mediaFileUrlRetriever.Retrieve(image).RelativePath,
                Title = properties.Title,
                Days1 = properties.Days1,
                Day2 = properties.Day2,
                Day3 = properties.Day3,
                Value1 = properties.Value1,
                Value2 = properties.Value2,
                Value3 = properties.Value3
            });
        }
        private MediaFileInfo GetImage(ContactTimeWidgetProperties properties)
        {
            var imageGuid = properties.Image.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}