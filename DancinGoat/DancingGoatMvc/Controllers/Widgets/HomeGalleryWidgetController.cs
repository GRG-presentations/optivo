﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;

[assembly: RegisterWidget(HomeGalleryWidgetController.IDENTIFIER, typeof(HomeGalleryWidgetController), "{$HomeGallery$}", Description = "{$dancinggoatmvc.widget.homegallery.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class HomeGalleryWidgetController : WidgetController<HomeGalleryWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.HomeGalleryWidget";


        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public HomeGalleryWidgetController(
            IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<HomeGalleryWidgetProperties>();
            // Gets an instance of the 'SampleMediaLibrary' media library for the current site
            MediaLibraryInfo mediaLibrary = MediaLibraryInfoProvider.GetMediaLibraryInfo(properties.MediaName, SiteContext.CurrentSiteName);

            // Gets a collection of media files with the .jpg extension from the media library
            IEnumerable<MediaFileInfo> mediaLibraryFiles = MediaFileInfoProvider.GetMediaFiles()
                .WhereEquals("FileLibraryID", mediaLibrary.LibraryID)
                .WhereEquals("FileExtension", ".jpg");

            IEnumerable<MediaFileInfo> mediaLibraryFiles2 = MediaFileInfoProvider.GetMediaFiles()
                .WhereEquals("FileLibraryID", mediaLibrary.LibraryID)
                .WhereEquals("FileExtension", ".mp4");

            // Prepares a collection of view models containing required data of the media files
            IEnumerable<MediaFileViewModel> model = mediaLibraryFiles.Select(
                    mediaFile => new MediaFileViewModel
                    {
                        FileTitle = mediaFile.FileTitle,
                        DirectUrl = MediaLibraryHelper.GetDirectUrl(mediaFile),
                        PermanentUrl = MediaLibraryHelper.GetPermanentUrl(mediaFile)
                    }
            );

            IEnumerable<MediaFileViewModel> model2 = mediaLibraryFiles2.Select(
                    mediaFile => new MediaFileViewModel
                    {
                        FileTitle = mediaFile.FileTitle,
                        DirectUrl = MediaLibraryHelper.GetDirectUrl(mediaFile),
                        PermanentUrl = MediaLibraryHelper.GetPermanentUrl(mediaFile)
                    }
            );

            return PartialView("Widgets/_DancingGoat_General_HomeGalleryWidget", new HomeGalleryWidgetViewModel
            {
                Images = model.Select(mediaFile => new MediaFileViewModel { 
                    FileTitle = mediaFile.FileTitle,
                    DirectUrl = mediaFile.DirectUrl,
                    PermanentUrl= mediaFile.PermanentUrl
                }),
                Videos = model2.Select(mediaFile => new MediaFileViewModel
                {
                    FileTitle = mediaFile.FileTitle,
                    DirectUrl = mediaFile.DirectUrl,
                    PermanentUrl = mediaFile.PermanentUrl
                })
            });
        }
    }
}