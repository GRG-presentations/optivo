﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;

[assembly: RegisterWidget(HomeBrandWidgetController.IDENTIFIER, typeof(HomeBrandWidgetController), "{$HomeBrand$}", Description = "{$dancinggoatmvc.widget.homebrand.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class HomeBrandWidgetController : WidgetController<HomeBrandWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.HomeBrandWidget";


        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public HomeBrandWidgetController(
            IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<HomeBrandWidgetProperties>();
            var image = GetImage(properties);

            outputCacheDependencies.AddDependencyOnInfoObject<MediaFileInfo>(image?.FileGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_HomeBrandWidget", new HomeBrandWidgetViewModel
            {
                Title = properties.Title,
                ColorRGBA = properties.ColorRGBA,
                Text = properties.Text,
                Image = image == null ? null : mediaFileUrlRetriever.Retrieve(image).RelativePath,
                Overview = properties.Overview,
                Gallery = properties.Gallery,
                Location = properties.Location,
                Specification = properties.Specification,
                Availability = properties.Availability,
                DownloadBrochure = properties.DownloadBrochure,
                Booking = properties.Booking
            });
        }

        private MediaFileInfo GetImage(HomeBrandWidgetProperties properties)
        {
            var imageGuid = properties.Image.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (imageGuid == Guid.Empty)
            {
                return null;
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}