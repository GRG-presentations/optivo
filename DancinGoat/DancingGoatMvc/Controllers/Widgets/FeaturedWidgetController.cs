﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;
using System.Collections.Generic;
using CMS.DocumentEngine;
using DancingGoat.Models.Testimonials;
using CMS.DocumentEngine.Types.DancingGoatMvc;
using DancingGoat.Models.PageTemplates;

[assembly: RegisterWidget(FeaturedWidgetController.IDENTIFIER, typeof(FeaturedWidgetController), "{$Featured$}", Description = "{$dancinggoatmvc.widget.featured.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class FeaturedWidgetController : WidgetController<FeaturedWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.FeaturedWidget";

        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly ITestimonialRepository testimonialRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IComponentPropertiesRetriever propertiesRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public FeaturedWidgetController(
           IPageDataContextRetriever pageDataContextRetriever,
            IOutputCacheDependencies outputCacheDependencies,
            IPageUrlRetriever pageUrlRetriever,
            ITestimonialRepository testimonialRepository,
            IComponentPropertiesRetriever propertiesRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.testimonialRepository = testimonialRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.propertiesRetriever = propertiesRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = propertiesRetriever.Retrieve<FeaturedWidgetProperties>();
            var listings = ListingProvider.GetListings();
            IEnumerable<Plot> plots = PlotProvider.GetPlots();
            IEnumerable<PlotsViewModel> PlotData = plots.Select(plot => PlotsViewModel.GetViewModel((Plot)plot, pageUrlRetriever, attachmentUrlRetriever));
            IEnumerable<ListViewModel> fullListing = listings.Select(item => ListViewModel.GetViewModel((Listing)item, pageUrlRetriever, attachmentUrlRetriever));
            var data = FeaturedWidgetViewModel.GetViewModel(PlotData , fullListing);
            return PartialView("Widgets/_DancingGoat_General_FeaturedWidget", data);
        }
    }
}