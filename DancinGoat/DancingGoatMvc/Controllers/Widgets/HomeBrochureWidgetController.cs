﻿using System;
using System.Linq;
using System.Web.Mvc;

using CMS.MediaLibrary;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

using DancingGoat.Controllers.Widgets;
using DancingGoat.Infrastructure;
using DancingGoat.Models.Widgets;
using DancingGoat.Repositories;

[assembly: RegisterWidget(HomeBrochureWidgetController.IDENTIFIER, typeof(HomeBrochureWidgetController), "{$HomeBrochure$}", Description = "{$dancinggoatmvc.widget.homebrochure.description$}", IconClass = "icon-badge")]
namespace DancingGoat.Controllers.Widgets
{
    public class HomeBrochureWidgetController : WidgetController<HomeBrochureWidgetProperties>
    {
        /// <summary>
        /// Widget identifier.
        /// </summary>
        public const string IDENTIFIER = "DancingGoat.General.HomeBrochureWidget";


        private readonly IMediaFileRepository mediaFileRepository;
        private readonly IOutputCacheDependencies outputCacheDependencies;
        private readonly IComponentPropertiesRetriever componentPropertiesRetriever;
        private readonly IMediaFileUrlRetriever mediaFileUrlRetriever;


        /// <summary>
        /// Creates an instance of <see cref="BannerWidgetController"/> class.
        /// </summary>
        public HomeBrochureWidgetController(
            IMediaFileRepository mediaFileRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IComponentPropertiesRetriever componentPropertiesRetriever,
            IMediaFileUrlRetriever mediaFileUrlRetriever)
        {
            this.mediaFileRepository = mediaFileRepository;
            this.outputCacheDependencies = outputCacheDependencies;
            this.componentPropertiesRetriever = componentPropertiesRetriever;
            this.mediaFileUrlRetriever = mediaFileUrlRetriever;
        }
        // GET: HomeMapWidget
        public ActionResult Index()
        {
            var properties = componentPropertiesRetriever.Retrieve<HomeBrochureWidgetProperties>();
            var DownloadDevelopmentBrochure = GetImage(properties, "brochure");
            var DownloadShared = GetImage(properties, "shared");
            var DownloadPrivate = GetImage(properties, "private");
            var DownloadRent = GetImage(properties, "rent");

            outputCacheDependencies.AddDependencyOnInfoObject<MediaFileInfo>(DownloadDevelopmentBrochure?.FileGUID ?? Guid.Empty);

            return PartialView("Widgets/_DancingGoat_General_HomeBrochureWidget", new HomeBrochureWidgetViewModel
            {
                Title = properties.Title,
                Text = properties.Text,
                ColorRGBA= properties.ColorRGBA,
                DevelopmentBrochure = properties.DevelopmentBrochure,
                SharedOwnershipFloorplans = properties.SharedOwnershipFloorplans,
                PrivateSaleFloorplans = properties.PrivateSalefloorplans,
                RentToBuyFloorplans = properties.RentToBuyFloorplans,
                DownloadDevelopmentBrochure = DownloadDevelopmentBrochure == null ? null : mediaFileUrlRetriever.Retrieve(DownloadDevelopmentBrochure).RelativePath,
                DownloadSharedOwnershipFloorplans = DownloadShared == null ? null : mediaFileUrlRetriever.Retrieve(DownloadShared).RelativePath,
                DownloadPrivateSaleFloorplans = DownloadPrivate == null ? null : mediaFileUrlRetriever.Retrieve(DownloadPrivate).RelativePath,
                DownloadRentToBuyFloorplans = DownloadRent == null ? null : mediaFileUrlRetriever.Retrieve(DownloadRent).RelativePath,
            });
        }

        private MediaFileInfo GetImage(HomeBrochureWidgetProperties properties, string type)
        {
            var imageGuid = properties.File1.FirstOrDefault()?.FileGuid ?? Guid.Empty;
            if (type == "brochure")
            {
                imageGuid = properties.File1.FirstOrDefault()?.FileGuid ?? Guid.Empty;
                if (imageGuid == Guid.Empty)
                {
                    return null;
                }
            }
            if (type == "shared")
            {
                imageGuid = properties.File2.FirstOrDefault()?.FileGuid ?? Guid.Empty;
                if (imageGuid == Guid.Empty)
                {
                    return null;
                }
            }
            if (type == "private")
            {
                imageGuid = properties.File3.FirstOrDefault()?.FileGuid ?? Guid.Empty;
                if (imageGuid == Guid.Empty)
                {
                    return null;
                }
            }
            if (type == "rent")
            {
                imageGuid = properties.File4.FirstOrDefault()?.FileGuid ?? Guid.Empty;
                if (imageGuid == Guid.Empty)
                {
                    return null;
                }
            }

            return mediaFileRepository.GetMediaFile(imageGuid, SiteContext.CurrentSiteID);
        }
    }
}