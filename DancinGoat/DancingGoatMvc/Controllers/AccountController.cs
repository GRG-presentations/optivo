﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using CMS.Activities.Loggers;
using CMS.Base.UploadExtensions;
using CMS.ContactManagement;
using CMS.Core;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;

using Kentico.Content.Web.Mvc;
using Kentico.Membership;
using Kentico.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using DancingGoat.Models.Account;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.IO;
using RestSharp;
using Newtonsoft.Json.Linq;

namespace DancingGoat.Controllers
{
    public class Profile
    {
        public int id { get; set; }
    }

    public class RegisterProfile
    {
            public string firstName { get; set; }
          public string surname { get; set; }
          public string email { get; set; }
          public string password { get; set; }
          public string mobile { get; set; }
          public string income { get; set; }
          public string savings { get; set; }
          public Array boroughsInterestedIn { get; set; }
          public string homeBorough { get; set; }
          public string workBorough { get; set; }
          public string marketingMaterial { get; set; }
          public string newsletter { get; set; }
          public string privacyPolicy { get; set; }
          public string canEmail { get; set; }
          public string canText { get; set; }
}
    public class AccountController : Controller
    {
        private readonly IMembershipActivityLogger membershipActivitiesLogger;
        private readonly IAvatarService avatarService;
        HttpClientHandler _clientHandler = new HttpClientHandler();

        public KenticoUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<KenticoUserManager>();
            }
        }


        public KenticoSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<KenticoSignInManager>();
            }
        }


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        public AccountController(IMembershipActivityLogger membershipActivitiesLogger, IAvatarService avatarService)
        {
            this.membershipActivitiesLogger = membershipActivitiesLogger;
            this.avatarService = avatarService;

            _clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        }


        // GET: Account/Dashboard
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Registration()
        {
            var model = new RegisterViewModel();
            return PartialView("_Register", model);
        }

        // GET: Account/Login
        public ActionResult Login()
        {
            return View();
        }

        // GET: Account/Forgot Password
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //Get: Account/LoginRegister
        public ActionResult LoginRegister()
        {
            return View();
        }

        // POST: Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var signInResult = SignInStatus.Failure;
            var jObject = new JObject();

            string url = String.Format("https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/customer-login");

            //previous sign in code
            //signInResult = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.StaySignedIn, false);

            var client = new RestClient(url + "?emailAddress=" + model.EmailAddress + "&password=" + model.Password);
            var JsonString = JsonConvert.SerializeObject(model);
            //// for First Applicatn
            var request = new RestRequest(Method.GET);
            //request.AddObject(model.Responses[0]);
            request.AddHeader("Authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");

            IRestResponse response = null;
            try
            {
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                response = client.Execute(request);

                // Extracting output data from received response
                string apiResponse = response.Content;

                // Parsing JSON content into element-node JObject
                jObject = JObject.Parse(response.Content);

                //Extracting Node element using Getvalue method
                //string city = jObject.GetValue("City").ToString();
                if (jObject.GetValue("message").ToString() == "Ok")
                {
                    signInResult = SignInStatus.Success;
                }

            }
            catch (Exception ex)
            {
                Service.Resolve<IEventLogService>().LogException("AccountController", "LoginRegister", ex);
                throw ex;
            }

            if (signInResult == SignInStatus.Success)
            {
                ContactManagementContext.UpdateUserLoginContact(model.EmailAddress);

                membershipActivitiesLogger.LogLogin(model.EmailAddress);

                var decodedReturnUrl = Server.UrlDecode(returnUrl);
                if (!string.IsNullOrEmpty(decodedReturnUrl) && Url.IsLocalUrl(decodedReturnUrl))
                {
                    return Redirect(decodedReturnUrl);
                }
                var data = jObject.GetValue("customerProfile").ToString();
                var data1 = JsonConvert.DeserializeObject<Profile>(data); ;
                Session["CurrentUserInfo"] = data1.id;

                return Redirect(Url.Kentico().PageUrl(ContentItemIdentifiers.HOME));
            }

            if (signInResult == SignInStatus.LockedOut)
            {
                ModelState.AddModelError(String.Empty, ResHelper.GetString("DancingGoatMvc.RequireConfirmedAccount"));
            }
            else
            {
                ModelState.AddModelError(String.Empty, ResHelper.GetString("login_failuretext"));
            }

            return View("Login");
        }


        // POST: Account/Logout
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return Redirect(Url.Kentico().PageUrl(ContentItemIdentifiers.HOME));
        }


        // GET: Account/RetrievePassword
        public ActionResult RetrievePassword()
        {
            return PartialView("_RetrievePassword");
        }


        // POST: Account/RetrievePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RetrievePassword(RetrievePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_RetrievePassword", model);
            }

            var user = UserManager.FindByEmail(model.Email);
            if (user != null)
            {
                var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var url = Url.Action("ResetPassword", "Account", new { userId = user.Id, token }, RequestContext.URL.Scheme);

                await UserManager.SendEmailAsync(user.Id, ResHelper.GetString("DancingGoatMvc.PasswordReset.Email.Subject"),
                    String.Format(ResHelper.GetString("DancingGoatMvc.PasswordReset.Email.Body"), url));
            }

            return Content(ResHelper.GetString("DancingGoatMvc.PasswordReset.EmailSent"));
        }


        // GET: Account/ResetPassword
        public ActionResult ResetPassword(int? userId, string token)
        {
            if (!userId.HasValue || String.IsNullOrEmpty(token))
            {
                return HttpNotFound();
            }

            if (!VerifyPasswordResetToken(userId.Value, token))
            {
                return View("ResetPasswordInvalidToken");
            }

            var model = new ResetPasswordViewModel()
            {
                UserId = userId.Value,
                Token = token
            };

            return View(model);
        }


        // POST: Account/ResetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (ResetUserPassword(model.UserId, model.Token, model.Password).Succeeded)
            {
                return View("ResetPasswordSucceeded");
            }

            return View("ResetPasswordInvalidToken");
        }


        // GET: Account/Register
        public ActionResult Register()
        {
            return View();
        }


        // POST: Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            /*if (!ModelState.IsValid)
            {
                return View(model);
            }*/

            //var user = new User
            //{
            //    Email = model.Email,
            //    FirstName = model.FirstName,
            //    LastName = model.LastName,
            //    EmailConfirmation = model.EmailConfirmation,
            //    FullName =  UserInfoProvider.GetFullName( model.FirstName, null, model.LastName),
            //    //Enabled = true

            //};
            var user = new RegisterProfile
            {
                firstName = model.FirstName,
                surname = model.LastName,
                mobile = model.Telephone,
                savings = model.Savings,
                income = model.AnnualHouseholdIncome,
                email = model.EmailConfirmation,
                password = model.Password,
            };

            var registerResult = new IdentityResult();

            var client = new RestClient("https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/register-customer");
            var JsonString = JsonConvert.SerializeObject(user);
            //// for First Applicatn
            var request = new RestRequest(Method.PUT);
            //request.AddObject(model.Responses[0]);
            request.AddHeader("Authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(JsonString);
            string result = "";
            try
            {
                var response = client.Execute(request);
                //Previous Code 
                //registerResult = await UserManager.CreateAsync(user, model.Password);
                result = response.StatusCode.ToString();

                ////using (var httpClient = new HttpClient(_clientHandler))
                ////{
                ////    StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                ////    //WebHeaderCollection myWebHeaderCollection = request.Headers;
                ////    //myWebHeaderCollection.Add("authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
                ////    //request.Method = "POST";
                ////    using (var response = await httpClient.PutAsync("https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/register-customer/", content))
                ////    {
                ////        string apiResponse = await response.Content.ReadAsStringAsync();
                ////        //_oTodoItem = JsonConvert.DeserializeObject<TodoItem>(apiResponse);
                ////    }
                ////}
                ///

                //Reservation receivedReservation = new Reservation();
                /*using (var httpClient = new HttpClient())
                {
                    var content = new MultipartFormDataContent();
                    content.Add(new StringContent(model.FirstName.ToString()), "");
                    content.Add(new StringContent(model.LastName.ToString()), "Name");
                    content.Add(new StringContent(model.UserName.ToString()), "StartLocation");
                    content.Add(new StringContent(model.EmailConfirmation.ToString()), "EndLocation");
                    content.Add(new StringContent(model.Password.ToString()), "");
                    content.Add(new StringContent(model.PasswordConfirmation.ToString()), "");
                    content.Add(new StringContent(model.Telephone.ToString()), "");
                    content.Add(new StringContent(model.LiveLocation.ToString()), "");
                    content.Add(new StringContent(model.WorkLocation.ToString()), "");
                    content.Add(new StringContent(model.AnnualHouseholdIncome.ToString()), "");
                    content.Add(new StringContent(model.Savings.ToString()), "");
                    content.Add(new StringContent(model.HearAboutUs.ToString()), "");

                    *//*using (var response = await httpClient.PutAsync("https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/register-customer", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ViewBag.Result = "Success";
                        //receivedReservation = JsonConvert.DeserializeObject<Reservation>(apiResponse);
                    }*//*
                    
                }*/


            }
            catch (Exception ex)
            {
                Service.Resolve<IEventLogService>().LogException("AccountController", "Register", ex);
                ModelState.AddModelError(String.Empty, ResHelper.GetString("register_failuretext"));
            }

            if (result == "OK")
            {
                membershipActivitiesLogger.LogRegistration(model.UserName);
                var signInResult = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);

                if (signInResult == SignInStatus.Success)
                {
                    ContactManagementContext.UpdateUserLoginContact(model.UserName);

                    SendRegistrationSuccessfulEmail(model.UserName);
                    membershipActivitiesLogger.LogLogin(model.UserName);

                    return Redirect(Url.Kentico().PageUrl(ContentItemIdentifiers.HOME));
                }

                //if (signInResult == SignInStatus.LockedOut)
                //{
                //    if (user.WaitingForApproval)
                //    {
                //        SendWaitForApprovalEmail(user.Email);
                //    }

                //    return RedirectToAction("RequireConfirmedAccount");
                //}
            }

            foreach (var error in registerResult.Errors)
            {
                ModelState.AddModelError(String.Empty, error);
            }

            return View("Login");
        }


        // GET: Account/YourAccount
        [Authorize]
        public ActionResult YourAccount(bool avatarUpdateFailed = false)
        {
            var model = new YourAccountViewModel
            {
                User = UserManager.FindByName(User.Identity.Name),
                AvatarUpdateFailed = avatarUpdateFailed
            };

            return View(model);
        }


        // GET: Account/Edit
        [Authorize]
        public ActionResult Edit()
        {
            var user = UserManager.FindByName(User.Identity.Name);
            var model = new PersonalDetailsViewModel(user);
            return View(model);
        }


        // POST: Account/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(PersonalDetailsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                return View(model);
            }

            try
            {
                var user = UserManager.FindByName(User.Identity.Name);

                // Set full name only if it was automatically generated
                if (user.FullName == UserInfoProvider.GetFullName(user.FirstName, null, user.LastName))
                {
                    user.FullName = UserInfoProvider.GetFullName(model.FirstName, null, model.LastName);
                }

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;

                await UserManager.UpdateAsync(user);

                return RedirectToAction("YourAccount");
            }
            catch (Exception ex)
            {
                Service.Resolve<IEventLogService>().LogException("AccountController", "Edit", ex);
                ModelState.AddModelError(String.Empty, ResHelper.GetString("DancingGoatMvc.YourAccount.SaveError"));

                model.UserName = User.Identity.Name;
                return View(model);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult ChangeAvatar(HttpPostedFileBase avatarUpload)
        {
            object routevalues = null;

            if (avatarUpload != null && avatarUpload.ContentLength > 0)
            {
                var user = UserManager.FindByName(User.Identity.Name);
                if (!avatarService.UpdateAvatar(avatarUpload.ToUploadedFile(), user.Id, SiteContext.CurrentSiteName))
                {
                    routevalues = new { avatarUpdateFailed = true };
                }
            }

            return RedirectToAction("YourAccount", routevalues);
        }


        // GET: Account/RequireConfirmedAccount
        [HttpGet]
        public ActionResult RequireConfirmedAccount()
        {
            return View();
        }


        /// <summary>
        /// Verifies if user's password reset token is valid.
        /// </summary>
        /// <param name="userId">User ID.</param>
        /// <param name="token">Password reset token.</param>
        /// <returns>True if user's password reset token is valid, false when user was not found or token is invalid or has expired.</returns>
        private bool VerifyPasswordResetToken(int userId, string token)
        {
            try
            {
                return UserManager.VerifyUserToken(userId, "ResetPassword", token);
            }
            catch (InvalidOperationException)
            {
                // User with given userId was not found
                return false;
            }
        }


        /// <summary>
        /// Reset user's password.
        /// </summary>
        /// <param name="userId">User ID.</param>
        /// <param name="token">Password reset token.</param>
        /// <param name="password">New password.</param>
        private IdentityResult ResetUserPassword(int userId, string token, string password)
        {
            try
            {
                return UserManager.ResetPassword(userId, token, password);
            }
            catch (InvalidOperationException)
            {
                // User with given userId was not found
                return IdentityResult.Failed("UserId not found.");
            }
        }


        private void SendRegistrationSuccessfulEmail(string email)
        {
            var message = new IdentityMessage()
            {
                Destination = email,
                Subject = ResHelper.GetString("membership.registrationinfosubject"),
                Body = ResHelper.GetString("membership.registrationinfobody")
            };

            UserManager.EmailService.SendAsync(message);
        }


        private void SendWaitForApprovalEmail(string email)
        {
            var message = new IdentityMessage()
            {
                Destination = email,
                Subject = ResHelper.GetString("membership.registrationapprovalnoticesubject"),
                Body = ResHelper.GetStringFormat("membership.registrationapprovalnoticebody", SiteContext.CurrentSite.DisplayName)
            };

            UserManager.EmailService.SendAsync(message);
        }
    }
}