﻿using System.Web.Mvc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CMS.DocumentEngine.Types.DancingGoatMvc;

using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;

using DancingGoat.Controllers;
using DancingGoat.Infrastructure;
using DancingGoat.Models.News;
using DancingGoat.Models.References;
using DancingGoat.Repositories;
using CMS.DocumentEngine;
using DancingGoat.Models.Blogs;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;
using DancingGoat.Models.BuyingGuides;
using DancingGoat.Models.EducationalPage;
using LinqToTwitter;
using DancingGoat.Models.FaqPage;

[assembly: RegisterPageRoute(BuyingGuides.CLASS_NAME, typeof(BuyingGuidesController))]
namespace DancingGoat.Controllers
{
    public class BuyingGuidesController : Controller
    {
        private readonly IPageDataContextRetriever dataContextRetriever;
        private readonly IBuyingGuidesRepository buyingGuidesRepository;
        private readonly IEducationalPageRepository educationalPagesRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;
        private readonly IOutputCacheDependencies outputCacheDependencies;

        public BuyingGuidesController(IPageDataContextRetriever dataContextRetriever,
            IBuyingGuidesRepository buyingGuidesRepository,
            IEducationalPageRepository educationalPagesRepository,
            IPageUrlRetriever pageUrlRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever,
            IOutputCacheDependencies outputCacheDependencies)
        {
            this.dataContextRetriever = dataContextRetriever;
            this.buyingGuidesRepository = buyingGuidesRepository;
            this.educationalPagesRepository = educationalPagesRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
        }
        // GET: BuyingGuides
        public async Task<ActionResult> Index(CancellationToken cancellationToken)
        {
            var section = dataContextRetriever.Retrieve<TreeNode>().Page;
            var footerPropertiesItems = educationalPagesRepository.GetEducationalPageSections("/Educational-Pages");
            var buyingGuides = buyingGuidesRepository.GetSideStories(section.NodeAliasPath);
            var sideStories = await buyingGuidesRepository.GetSideStoriesAsync(section.NodeAliasPath, cancellationToken);
            var educationalPages = educationalPagesRepository.GetEducationalPages(section.NodeAliasPath);
            var sections = DocumentHelper.GetDocuments("DancingGoatMvc.EducationalPageSection").WhereStartsWith("NodeAliasPath", "/Educational-Pages");
            var sections1 = DocumentHelper.GetDocuments("DancingGoatMvc.FaqSection");

            outputCacheDependencies.AddDependencyOnPages(buyingGuides);

            BuyingGuidesViewModel mode = new BuyingGuidesViewModel()
            {
                Sections = sideStories.Select(story => BuyingGuidesSectionViewModel.GetViewModel(story, pageUrlRetriever, attachmentUrlRetriever)),
                EducationalsSections = sections.Select(item => EducationalPageSectionViewModel.GetViewModel((EducationalPageSection)item, pageUrlRetriever, attachmentUrlRetriever)),
                FaqSections = sections1.Select(item => FaqSectionViewModel.GetViewModel((FaqSection)item, pageUrlRetriever, attachmentUrlRetriever))
            };

            return View(mode);
        }
    }
}