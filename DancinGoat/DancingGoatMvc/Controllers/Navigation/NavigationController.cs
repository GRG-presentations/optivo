﻿using System.Linq;
using System.Net;
using System.Web.Mvc;

using DancingGoat.Infrastructure;
using DancingGoat.Models.Navigation;
using DancingGoat.Repositories;

using Kentico.Content.Web.Mvc;
using Kentico.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace DancingGoat.Controllers
{
    public class NavigationController : Controller
    {
        private readonly INavigationRepository navigationRepository;
        private readonly IPropertiesRepository propertiesRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IOutputCacheDependencies outputCacheDependencies;


        public NavigationController(INavigationRepository navigationRepository, IPropertiesRepository propertiesRepository, IOutputCacheDependencies outputCacheDependencies, IPageUrlRetriever pageUrlRetriever)
        {
            this.navigationRepository = navigationRepository;
            this.propertiesRepository = propertiesRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
        }


        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            var menuItems = navigationRepository.GetMenuItems();
            var menuItemsModel = menuItems.Select(menuItem => MenuItemModel.GetViewModel(menuItem, pageUrlRetriever));

            outputCacheDependencies.AddDependencyOnPages(menuItems);

            return PartialView("Navigation/_Menu", new MenuViewModel { Items = menuItemsModel });
        }


        [ChildActionOnly]
        public PartialViewResult Footer()
        {
            var footerNavigationItems = navigationRepository.GetFooterNavigationItems();
            var footerPropertiesItems = propertiesRepository.GetFooterPropertiesItems();
            var footerNavigationItemsModel = footerNavigationItems.Select(item => MenuItemModel.GetViewModel(item, pageUrlRetriever));
            var footerPropertiesItemsModel = footerPropertiesItems.Select(item => MenuItemModel.GetViewModel(item, pageUrlRetriever));

            outputCacheDependencies.AddDependencyOnPages(footerNavigationItems);

            return PartialView("Navigation/_Footer", new MenuViewModel { Items = footerNavigationItemsModel, propertiesItems = footerPropertiesItemsModel });
        }

        // POST: Account/Logout
        [HttpPost]
        public ActionResult Logout()
        {
            Session.Clear();
            return Redirect(Url.Kentico().PageUrl(ContentItemIdentifiers.HOME));
        }
    }
}