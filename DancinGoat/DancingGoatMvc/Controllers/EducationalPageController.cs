﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers;
using DancingGoat.Infrastructure;
using DancingGoat.Models.EducationalPage;
using DancingGoat.Models.References;
using DancingGoat.Repositories;

using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;

[assembly: RegisterPageRoute(EducationalPage.CLASS_NAME, typeof(EducationalPageController))]
namespace DancingGoat.Controllers
{
    public class EducationalPageController : Controller
    {
        private readonly IPageDataContextRetriever pageDataContextRetriever;
        private readonly IEducationalPageRepository educationalPageSectionRepository;
        private readonly IReferenceRepository referenceRepository;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;
        private readonly IOutputCacheDependencies outputCacheDependencies;

        public EducationalPageController(IPageDataContextRetriever pageDataContextRetriever,
            IEducationalPageRepository educationalPageSectionRepository,
            IReferenceRepository referenceRepository,
            IOutputCacheDependencies outputCacheDependencies,
            IPageUrlRetriever pageUrlRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriever = pageDataContextRetriever;
            this.educationalPageSectionRepository = educationalPageSectionRepository;
            this.referenceRepository = referenceRepository;
            this.pageUrlRetriever = pageUrlRetriever;
            this.outputCacheDependencies = outputCacheDependencies;
            this.attachmentUrlRetriever = attachmentUrlRetriever;

        }
        // GET: EducationalPage
        [OutputCache(CacheProfile = "Default")]
        public async Task<ActionResult> Index(CancellationToken cancellationToken)
        {
            var educationalPage = pageDataContextRetriever.Retrieve<EducationalPage>().Page;
            var educationalPageSections = await educationalPageSectionRepository.GetEducationalPageSectionsAsync(educationalPage.NodeAliasPath, cancellationToken);
            var reference = (await referenceRepository.GetReferencesAsync(educationalPage.NodeAliasPath, cancellationToken, 1)).FirstOrDefault();

            var viewModel = new EducationalPageViewModel
            {
                EducationalPageSections = educationalPageSections.Select(section => EducationalPageSectionViewModel.GetViewModel(section, pageUrlRetriever, attachmentUrlRetriever)),
                Reference = ReferenceViewModel.GetViewModel(reference, attachmentUrlRetriever)
            };

            outputCacheDependencies.AddDependencyOnPage(educationalPage);
            outputCacheDependencies.AddDependencyOnPages(educationalPageSections);
            outputCacheDependencies.AddDependencyOnPage(reference);

            return View(viewModel);
        }
    }
}