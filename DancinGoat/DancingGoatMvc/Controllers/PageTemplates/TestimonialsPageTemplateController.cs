﻿using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.PageTemplates;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageTemplate("DancingGoat.Testimonials", typeof(TestimonialsPageTemplateController), "{$Testimonials$}", Description = "{$dancinggoatmvc.pagetemplate.testimonials.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class TestimonialsPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public TestimonialsPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            var testimonial = pageDataContextRetriver.Retrieve<Testimonial>().Page;
            if (testimonial == null)
            {
                return HttpNotFound();
            }

            return View("PageTemplates/_Testimonials", TestimonialsViewModel.GetViewModel(testimonial, pageUrlRetriever, attachmentUrlRetriever));
        }
    }
}