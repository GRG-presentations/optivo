﻿using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.EducationalPage;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageTemplate("DancingGoat.MainPage", typeof(EducationalPageTemplateController), "{$MainPage$}", Description = "{$dancinggoatmvc.pagetemplate.mainpage.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class MainPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public MainPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            var data = pageDataContextRetriver.Retrieve<EducationalPageSection>().Page;
            if (data == null)
            {
                return HttpNotFound();
            }

            return View("PageTemplates/_MainPage", EducationalPageSectionViewModel.GetViewModel(data, pageUrlRetriever, attachmentUrlRetriever));
        }
    }
}