﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Mvc;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.PageTemplates;
using CMS.DocumentEngine;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;
using Newtonsoft.Json;
using System.Linq;

[assembly: RegisterPageTemplate("DancingGoat.Listing", typeof(ListingPageTemplateController), "{$Listing$}", Description = "{$dancinggoatmvc.pagetemplate.listing.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class ListingPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public ListingPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever,
            IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            var listing = pageDataContextRetriver.Retrieve<Listing>().Page;
            ListingViewModel model = ListingViewModel.GetViewModel(listing);
            if (listing == null)
            {
                return HttpNotFound();
            }

            // Applies the width specified through the parameter of the form control if it is valid
            const string url = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/get-properties-for-development/";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + model.DevelopmentId.ToString());
            WebHeaderCollection myWebHeaderCollection = request.Headers;
            myWebHeaderCollection.Add("authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.Method = "GET";

            var webResponse = request.GetResponse();
            var webStream = webResponse.GetResponseStream();
            var responseReader = new StreamReader(webStream);
            var response = responseReader.ReadToEnd();
            IEnumerable<Plots> model1 = JsonConvert.DeserializeObject<List<Plots>>(response);
            IEnumerable<Plot> plots = PlotProvider.GetPlots()
                        .WhereStartsWith("NodeAliasPath", "/Listings/"+ model.DevelopmentId.ToString());
            IEnumerable<PlotsViewModel> PlotData = plots.Select(plot => PlotsViewModel.GetViewModel((Plot)plot, pageUrlRetriever, attachmentUrlRetriever));
            PlotMinViewModel cardData = PlotMinViewModel.GetViewModel(model1.ToList());

            var listingFull = ListingFullViewModel.GetViewModel(listing, model1, PlotData, cardData);


            return View("PageTemplates/_Listing", listingFull);
        }
    }
}