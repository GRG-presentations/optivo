﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.EducationalPage;
using DancingGoat.Models.PageTemplates;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageTemplate("DancingGoat.Search", typeof(SearchPageTemplateController), "{$Search$}", Description = "{$dancinggoatmvc.pagetemplate.search.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class SearchPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public SearchPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            IEnumerable<TreeNode> listings = DocumentHelper.GetDocuments("DancingGoatMvc.Listing").OnCurrentSite().Published().ToList();
            IEnumerable<ListViewModel> fullListing = listings.Select(item => ListViewModel.GetViewModel((Listing)item, pageUrlRetriever, attachmentUrlRetriever));

            List<SearchSection> pageSection = SearchSectionProvider.GetSearchSections().ToList();
            if (pageSection == null)
            {
                return HttpNotFound();
            }
            var dataFull = SearchViewModel.GetViewModel((SearchSection)pageSection[0], pageUrlRetriever, attachmentUrlRetriever, fullListing);
            return View("PageTemplates/_Search", dataFull);
        }
    }
}