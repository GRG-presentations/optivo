﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.ContactPage;
using DancingGoat.Models.EducationalPage;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageTemplate("DancingGoat.ContactPage", typeof(ContactPageTemplateController), "{$ContactPage$}", Description = "{$dancinggoatmvc.pagetemplate.contactpage.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class ContactPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public ContactPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            List<ContactPageSection> pageSection = ContactPageSectionProvider.GetContactPageSections().ToList();
            if (pageSection == null)
            {
                return HttpNotFound();
            }
            var dataFull = ContactPageSectionViewModel.GetViewModel((ContactPageSection)pageSection[0], pageUrlRetriever, attachmentUrlRetriever);
            return View("PageTemplates/_ContactPage", ContactPageViewModel.GetViewModel(dataFull, attachmentUrlRetriever));
        }
    }
}