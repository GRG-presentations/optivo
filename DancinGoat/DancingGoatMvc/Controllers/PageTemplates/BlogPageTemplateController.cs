﻿using System.Web.Mvc;

using CMS.DocumentEngine.Types.DancingGoatMvc;

using DancingGoat.Controllers.PageTemplates;
using DancingGoat.Models.PageTemplates;

using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

[assembly: RegisterPageTemplate("DancingGoat.Blog", typeof(BlogPageTemplateController), "{$Blog$}", Description = "{$dancinggoatmvc.pagetemplate.blog.description$}", IconClass = "icon-l-text")]

namespace DancingGoat.Controllers.PageTemplates
{
    public class BlogPageTemplateController : PageTemplateController
    {
        private readonly IPageDataContextRetriever pageDataContextRetriver;
        private readonly IPageUrlRetriever pageUrlRetriever;
        private readonly IPageAttachmentUrlRetriever attachmentUrlRetriever;


        public BlogPageTemplateController(IPageDataContextRetriever pageDataContextRetriver, IPageUrlRetriever pageUrlRetriever, IPageAttachmentUrlRetriever attachmentUrlRetriever)
        {
            this.pageDataContextRetriver = pageDataContextRetriver;
            this.pageUrlRetriever = pageUrlRetriever;
            this.attachmentUrlRetriever = attachmentUrlRetriever;
        }


        public ActionResult Index()
        {
            var blog = pageDataContextRetriver.Retrieve<Blog>().Page;
            if (blog == null)
            {
                return HttpNotFound();
            }

            return View("PageTemplates/_Blog", BlogViewModel.GetViewModel(blog, pageUrlRetriever, attachmentUrlRetriever));
        }
    }
}