﻿using System.Web.Mvc;
using System;
using System.Collections.Generic;
using ApiCalling.Models;
using Newtonsoft.Json;
using RestSharp;

namespace DancingGoat.Controllers
{
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            bindList();
            if (Session["CurrentUserInfo"] != null)
            {
                return View();
            } else
            {
                return View("~/Views//Account/Login.cshtml");
            }
            
        }

        public ActionResult ApiCalling(string customerID)
        {
            finalResponse objModel = new finalResponse
            {
                Responses = new List<Response>()
            };
            try
            {
                string URL = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/get-customer-profile/" + customerID;
                string method = "GET";
                IRestResponse response = apiCall(URL, method, null);
                Response respObj = new Response();
                respObj = JsonConvert.DeserializeObject<Response>(response.Content);
                objModel.Responses.Add(respObj);
                objModel.Responses.Add(respObj);
                bindList();
            }
            catch (Exception ex)
            {
                // log exception here
            }

            return View("Index", objModel);
        }

        [HttpPost]
        public ActionResult Update(finalResponse model)
        {
            bindList();

            string URL = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/save-customer-profile/" + model.Responses[0].id;
            string method = "PUT";
            IRestResponse response = apiCall(URL, method, model.Responses[0]);

            // fro second applicant
            //URL = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/save-customer-profile/" + model.Responses[0].id;
            //method = "PUT";
            //response = apiCall(URL, method, model.Responses[1]);


            return View("Index");
        }


        public ActionResult AddAddress(string index)
        {
            bindList();
            ViewBag.Index = index;
            Response respObj = new Response
            {
                previousAddresses = new List<PreviousAddress>()
            };

            respObj.previousAddresses.Add(new PreviousAddress
            {
                previousAddressId = int.Parse(index)
            });

            finalResponse objModel = new finalResponse
            {
                Responses = new List<Response>()
            };


            objModel.Responses.Add(respObj);
            return PartialView("_previousAddresses", objModel);
        }

        public ActionResult AddAddress2(string index2)
        {
            bindList();
            ViewBag.Index2 = index2;
            Response respObj = new Response
            {
                previousAddresses = new List<PreviousAddress>()
            };

            respObj.previousAddresses.Add(new PreviousAddress
            {
                previousAddressId = int.Parse(index2)
            });

            finalResponse objModel = new finalResponse
            {
                Responses = new List<Response>()
            };


            objModel.Responses.Add(respObj);
            return PartialView("_previousAddresses", objModel);
        }

        public void bindList()
        {
            try
            {
                string URL = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/get-lists";
                string method = "GET";
                IRestResponse response = apiCall(URL, method, null);
                List<ListModel> ddlList = new List<ListModel>();
                ddlList = JsonConvert.DeserializeObject<List<ListModel>>(response.Content);


                foreach (var item in ddlList)
                {
                    URL = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.2/get-list-items/" + item.id;
                    method = "GET";
                    response = apiCall(URL, method, null);
                    List<DDLModel> _itemlist = new List<DDLModel>();
                    _itemlist = JsonConvert.DeserializeObject<List<DDLModel>>(response.Content);
                    if (item.name == "Economic Status")
                    {
                        ViewBag.EconomicStatus = _itemlist;

                    }
                    else if (item.name == "Ethnicity")
                    {

                        ViewBag.Ethnicity = _itemlist;
                    }
                    else if (item.name == "Gender Type")
                    {
                        ViewBag.GenderType = _itemlist;
                    }
                    else if (item.name == "Local Authority")
                    {
                        ViewBag.LocalAuthority = _itemlist;
                    }
                    else if (item.name == "Nationality")
                    {
                        ViewBag.Nationality = _itemlist;
                    }
                    else if (item.name == "Religion")
                    {
                        ViewBag.Religion = _itemlist;
                    }
                    else if (item.name == "Relationship Status")
                    {
                        ViewBag.RelationshipStatus = _itemlist;
                    }
                    else if (item.name == "Sexuality")
                    {
                        ViewBag.Sexuality = _itemlist;
                    }
                    else if (item.name == "Marital Status")
                    {
                        ViewBag.MaritalStatus = _itemlist;
                    }
                    else if (item.name == "Accommodation Types")
                    {
                        ViewBag.AccommodationTypes = _itemlist;
                    }

                }
                TempData["DDLLISTGET"] = "T";
                TempData["EconomicStatus"] = "asd";
            }
            catch (Exception e)
            {

                // log the exception
            }
        }

        public IRestResponse apiCall(string pURL, string pMethod, Response model)
        {
            var client = new RestClient(pURL);
            var request = new RestRequest();
            if (pMethod == "GET")
            {
                request.Method = Method.GET;
            }
            else if (pMethod == "PUT")
            {
                request.Method = Method.PUT;
            }
            else if (pMethod == "POST")
            {
                request.Method = Method.POST;
            }
            request.AddHeader("Authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");

            if (model != null)
            {
                request.AddJsonBody(model);
            }

            IRestResponse response;
            try
            {
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                response = client.Execute(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;


        }
    }
}