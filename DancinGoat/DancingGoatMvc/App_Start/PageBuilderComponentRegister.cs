﻿using DancingGoat;
using DancingGoat.Models.PageTemplates;
using DancingGoat.Models.Sections;
using DancingGoat.Models.Widgets;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

// Widgets
[assembly: RegisterWidget(ComponentIdentifiers.TESTIMONIAL_WIDGET, "{$dancinggoatmvc.widget.testimonial.name$}", typeof(TestimonialWidgetProperties), Description = "{$dancinggoatmvc.widget.testimonial.description$}", IconClass = "icon-right-double-quotation-mark")]
[assembly: RegisterWidget(ComponentIdentifiers.CTA_BUTTON_WIDGET, "{$dancinggoatmvc.widget.ctabutton.name$}", typeof(CTAButtonWidgetProperties), Description = "{$dancinggoatmvc.widget.ctabutton.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.HOME_MAP_WIDGET, "{$dancinggoatmvc.widget.homemap.name$}", typeof(HomeMapWidgetProperties), Description = "{$dancinggoatmvc.widget.homemap.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.HOME_DETAILS_WIDGET, "{$dancinggoatmvc.widget.homedetails.name$}", typeof(HomeDetailsWidgetProperties), Description = "{$dancinggoatmvc.widget.homedetails.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.HOME_BRAND_WIDGET, "{$dancinggoatmvc.widget.homebrand.name$}", typeof(HomeBrandWidgetProperties), Description = "{$dancinggoatmvc.widget.homebrand.description$}", IconClass = "icon-rectangle-a")]

[assembly: RegisterWidget(ComponentIdentifiers.HOME_SPECIFICATION_WIDGET, "{$HomeSpecificationInfo$}", typeof(HomeSpecificationWidgetProperties), Description = "{$dancinggoatmvc.widget.homespecification.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.HOME_REGISTER_WIDGET, "{$dancinggoatmvc.widget.homeregister.name$}", typeof(HomeRegisterWidgetProperties), Description = "{$dancinggoatmvc.widget.homeregister.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.HOME_BROCHURE_WIDGET, "{$dancinggoatmvc.widget.homebrochure.name$}", typeof(HomeBrochureWidgetProperties), Description = "{$dancinggoatmvc.widget.homebrochure.description$}", IconClass = "icon-rectangle-a")]

[assembly: RegisterWidget(ComponentIdentifiers.VIDEO_IMAGE_BOX_WIDGET, "{$VIDEOIMAGEBOXWIDGET$}", typeof(VideoImageBoxWidgetProperties), Description = "{$dancinggoatmvc.widget.videoimagebox.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.TESTIMONIALS_FEATURE_WIDGET, "{$TESTIMONIALSFEATURE$}", typeof(TestimonialsFeatureWidgetProperties), Description = "{$dancinggoatmvc.widget.testimonialsfeature.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.ACCORDION_CONTAINER_WIDGET, "{$ACCORDIONCONTAINER$}", typeof(AccordionContainerWidgetProperties), Description = "{$dancinggoatmvc.widget.accordioncontainer.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.ACCORDION_ITEM_WIDGET, "{$ACCORDIONITEM$}", typeof(AccordionItemWidgetProperties), Description = "{$dancinggoatmvc.widget.accordionitem.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.MORE_FROM_OPTIVO_WIDGET, "{$MOREFROMOPTIVO$}", typeof(MoreFromOptivoWidgetProperties), Description = "{$dancinggoatmvc.widget.morefromoptivo.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.TESTIMONIALS_LIST_WIDGET, "{$TESTIMONIALSLIST$}", typeof(TestimonialsListWidgetProperties), Description = "{$dancinggoatmvc.widget.testimonialslist.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.FEATURED_WIDGET, "{$FEATURED$}", typeof(FeaturedWidgetProperties), Description = "{$dancinggoatmvc.widget.featured.description$}", IconClass = "icon-rectangle-a")]

[assembly: RegisterWidget(ComponentIdentifiers.CONTACT_CONTAINER_WIDGET, "{$CONTACTCONTAINER$}", typeof(ContactContainerWidgetProperties), Description = "{$dancinggoatmvc.widget.contactcontainer.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.CONTACT_TIME_WIDGET, "{$CONTACTTIME$}", typeof(ContactTimeWidgetProperties), Description = "{$dancinggoatmvc.widget.contacttime.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.CONTACT_CARD_WIDGET, "{$CONTACTCARD$}", typeof(ContactCardWidgetProperties), Description = "{$dancinggoatmvc.widget.contactcard.description$}", IconClass = "icon-rectangle-a")]

/*[assembly: RegisterWidget(ComponentIdentifiers.TWO_IMAGE_WIDGET, "{$TWOIMAGE$}", typeof(TwoImageWidgetProperties), Description = "{$dancinggoatmvc.widget.twoimage.description$}", IconClass = "icon-rectangle-a")]
[assembly: RegisterWidget(ComponentIdentifiers.MEET_THE_TEAM_WIDGET, "{$MEETTHETEAM$}", typeof(MeetTheTeamWidgetProperties), Description = "{$dancinggoatmvc.widget.meettheteam.description$}", IconClass = "icon-rectangle-a")]*/

[assembly: RegisterWidget(ComponentIdentifiers.HOME_GALLERY_WIDGET, "{$dancinggoatmvc.widget.homegallery.name$}", typeof(HomeGalleryWidgetProperties), Description = "{$dancinggoatmvc.widget.homegallery.description$}", IconClass = "icon-rectangle-a")]

// Sections
[assembly: RegisterSection(ComponentIdentifiers.SINGLE_COLUMN_SECTION, "{$dancinggoatmvc.section.singlecolumn.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.singlecolumn.description$}", IconClass = "icon-square")]
[assembly: RegisterSection(ComponentIdentifiers.TWO_COLUMN_SECTION, "{$dancinggoatmvc.section.twocolumn.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.twocolumn.description$}", IconClass = "icon-l-cols-2")]
[assembly: RegisterSection(ComponentIdentifiers.THREE_COLUMN_SECTION, "{$dancinggoatmvc.section.threecolumn.name$}", typeof(ThreeColumnSectionProperties), Description = "{$dancinggoatmvc.section.threecolumn.description$}", IconClass = "icon-l-cols-3")]

// Page templates
[assembly: RegisterPageTemplate(ComponentIdentifiers.LANDING_PAGE_SINGLE_COLUMN_TEMPLATE, "{$dancinggoatmvc.pagetemplate.landingpagesinglecolumn.name$}", typeof(LandingPageSingleColumnProperties), Description = "{$dancinggoatmvc.pagetemplate.landingpagesinglecolumn.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.EDUCATIONAL_PAGE_TEMPLATE, "{$EducationalPagee$}", typeof(EducationalPageProperties), Description = "{$dancinggoatmvc.pagetemplate.educationalpage.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.MAIN_PAGE_TEMPLATE, "{$MainPage$}", typeof(MainPageProperties), Description = "{$dancinggoatmvc.pagetemplate.mainpage.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.BLOG_TEMPLATE, "{$Blog$}", typeof(BlogProperties), Description = "{$dancinggoatmvc.pagetemplate.blog.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.TESTIMONIAL_TEMPLATE, "{$Testimonial$}", typeof(TestimonialProperties), Description = "{$dancinggoatmvc.pagetemplate.testimonial.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.FAQ_TEMPLATE, "{$Faq$}", typeof(FaqProperties), Description = "{$dancinggoatmvc.pagetemplate.faq.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.LISTING_TEMPLATE, "{$Listing$}", typeof(ListingProperties), Description = "{$dancinggoatmvc.pagetemplate.listing.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.TESTIMONIALS_TEMPLATE, "{$Testimonials$}", typeof(TestimonialsProperties), Description = "{$dancinggoatmvc.pagetemplate.testimonials.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.SEARCH_TEMPLATE, "{$Search$}", typeof(SearchProperties), Description = "{$dancinggoatmvc.pagetemplate.search.description$}")]
[assembly: RegisterPageTemplate(ComponentIdentifiers.CONTACT_PAGE_TEMPLATE, "{$ContactPage$}", typeof(ContactPageProperties), Description = "{$dancinggoatmvc.pagetemplate.contactpage.description$}")]
