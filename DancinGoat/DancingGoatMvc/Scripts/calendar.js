﻿/*Calendar functions*/
$(window).on({
    load: function () {

        /* Calendars
        -----------------------------------------------------------*/
        var normalDates = {};
        normalDates[new Date('11/03/2017')] = new Date('11/09/2017');
        normalDates[new Date('11/03/2017')] = new Date('11/09/2017');

        var peakDates = [];
        peakDates[new Date('03/10/2017')] = new Date('03/15/2017');
        peakDates[new Date('03/13/2017')] = new Date('03/12/2017');

        //define range
        var fromDate = new Date("03/10/2017");
        var toDate = new Date("03/15/2017");

        $('#calendar-1').datepicker({
            dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            firstDay: 1,
            showCurrentAtPos: 0,
            beforeShowDay: function (date) {

                var normalRate = normalDates[date];
                var peakRate = peakDates[date];

                if (normalRate) {
                    //if($.inArray(date, normal_rate_dates)) {
                    return [true, 'rate-default', 'tooltip text'];
                }
                else if (date >= fromDate && date <= toDate) {
                    // if($.inArray(date, normal_rate_dates)) {
                    return [true, 'rate-blue', 'tooltip text'];

                }
                else {
                    // default
                    return [true, '', ''];
                }
            }
        });
        /* End Of Calendars
        -----------------------------------------------------------*/
    },
});
  /*end of calendar*/