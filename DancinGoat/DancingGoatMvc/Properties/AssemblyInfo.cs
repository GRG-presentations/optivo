using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DancingGoat")]
[assembly: AssemblyDescription("A sample ASP.NET MVC 5 application that demonstrates integration with Kentico.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("819c7a3d-80fa-4e95-ab40-22553acc6744")]

[assembly: AssemblyProduct("DancingGoat")]
[assembly: AssemblyCopyright("© 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("13.0.0.0")]
[assembly: AssemblyFileVersion("13.0.0.21407")]
[assembly: AssemblyInformationalVersion("13.0.0")]

[assembly: InternalsVisibleTo("DancingGoat.Tests")]