﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using System.IO;
using Newtonsoft.Json;

namespace CMSApp.CMSFormControls
{
    public class Listing
    {
        public string developmentId { get; set; }
        public string developmentName { get; set; }
    }
    public partial class ListingSelector : FormEngineUserControl
    {
        static HttpClient client = new HttpClient();
        /// <summary>
        /// Gets or sets the value entered into the field, a hexadecimal color code in this case.
        /// </summary>
        public override object Value
        {
            get
            {
                return drpColor.SelectedValue;
            }
            set
            {
                // Selects the matching value in the drop-down
                EnsureItems();
                drpColor.SelectedValue = System.Convert.ToString(value);
            }
        }

        /// <summary>
        /// Property used to access the Width parameter of the form control.
        /// </summary>
        public int SelectorWidth
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SelectorWidth"), 0);
            }
            set
            {
                SetValue("SelectorWidth", value);
            }
        }

        /// <summary>
        /// Returns an array of values of any other fields returned by the control.
        /// </summary>
        /// <returns>It returns an array where the first dimension is the field name and the second is its value.</returns>
        public override object[,] GetOtherValues()
        {
            object[,] array = new object[1, 2];
            array[0, 0] = "ProductColor";
            array[0, 1] = drpColor.SelectedItem.Text;
            return array;
        }

        /// <summary>
        /// Returns true if a color is selected. Otherwise, it returns false and displays an error message.
        /// </summary>
        public override bool IsValid()
        {
            if ((string)Value != "")
            {
                return true;
            }
            else
            {
                // Sets the form control validation error message
                this.ValidationError = "Please choose a color.";
                return false;
            }
        }

        static async Task<Listing> GetListingAsync()
        {
            Listing product = null;
            HttpResponseMessage response = await client.GetAsync($"/api/v1.2/get-customer/0");
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsAsync<Listing>();
            }
            return product;
        }

        /// <summary>
        /// Sets up the internal DropDownList control.
        /// </summary>
        protected void EnsureItems()
        {
            // Applies the width specified through the parameter of the form control if it is valid
            const string url = "https://optivo.staging.fullcirclesoftware.co.uk/api/v1.1/developments";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebHeaderCollection myWebHeaderCollection = request.Headers;
            myWebHeaderCollection.Add("authorization", "2-CmI7c6JMdJjfOJ5L77LJeHMg0FW2sF1h0eKxWdaD7Guc14GwlmPXwYyX55zM");
            request.Method = "GET";

            var webResponse = request.GetResponse();
            var webStream = webResponse.GetResponseStream();
            var responseReader = new StreamReader(webStream);
            var response = responseReader.ReadToEnd();
            Console.WriteLine("Response: " + response);
            var model = JsonConvert.DeserializeObject<List<Listing>>(response);
            foreach (var item in model)
            {
                drpColor.Items.Add(new ListItem(item.developmentName, item.developmentId));
            }

            if (SelectorWidth > 0)
            {
                drpColor.Width = SelectorWidth;
            }

            // Generates the options in the drop-down list
            if (drpColor.Items.Count == 0)
            {
                drpColor.Items.Add(new ListItem("(select listing)", ""));
                drpColor.Items.Add(new ListItem("Listing 1", "#FF0000"));
                drpColor.Items.Add(new ListItem("Listing 2", "#00FF00"));
                drpColor.Items.Add(new ListItem("Listing 3", "#0000FF"));
            }
        }

        /// <summary>
        /// Handler for the Load event of the control.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {   
            // Initializes the drop-down list options
            /*RunAsync().GetAwaiter().GetResult();*/
            EnsureItems();
        }
    }
}