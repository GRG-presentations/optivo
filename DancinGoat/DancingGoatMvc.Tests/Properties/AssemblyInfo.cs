using System.Runtime.CompilerServices;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DancingGoat.Tests")]
[assembly: AssemblyDescription("Tests a sample ASP.NET MVC 5 application that demonstrates integration with Kentico.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("d633392e-bf25-417b-86de-979e15d675b7")]

[assembly: AssemblyProduct("DancingGoat.Tests")]
[assembly: AssemblyCopyright("© 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("13.0.0.0")]
[assembly: AssemblyFileVersion("13.0.0.21407")]
[assembly: AssemblyInformationalVersion("13.0.0")]